from core.api import APIWrapper

BASE_URL = 'http://127.0.0.1:8000/api/v1'
# BASE_URL = 'http://ec2-52-39-180-60.us-west-2.compute.amazonaws.com:8000/api/v1'

def get_countries_states_cities():
    api = APIWrapper(email='davem220@gmail.com', password='H2*Yzm7SL')
    login = api.login()
    country_list = api.countries().json()
    country_list = [country for country in country_list if country['code'] == 'US']
    states_dict ={}
    for country in country_list:
        states_dict[country['id']] = api.states(params={'country': country['id']}).json()

    cities = {}
    for state in states_dict[country['id']]:
        cities[state['id']] = api.cities(params={'state': state['id']}).json()

    return country_list, cities, states_dict

countries, cities, states_dict = get_countries_states_cities()
