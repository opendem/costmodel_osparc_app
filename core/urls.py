from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [

    url(r'^$', views.auth, name='auth'),
    url(r'^index/$', views.index, name='index'),

    url(r'^logout/$', views.logout, name='logout'),

    url(r'^profile/$', views.profile, name='profile'),
    url(r'^help/$', views.help_view, name='help'),
    # Password Reset #
    url(r'^reset_password_confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', 
        views.reset_password_confirm, 
        name='reset_password_confirm'
    ),

    url(r'^organizations/$', views.organization_list, name='organization_list'),
    url(r'^organizations/(?P<org_pk>\d+)/$', views.organization_detail, name='organization_detail'),
    url(r'^organizations/(?P<org_pk>new)/$', views.organization_create, name='organization_create'),

    url(r'^account_create/$', views.account_create, name='account_create'),
    url(r'^organizations/(?P<org_pk>\d+)/accounts/$', views.account_list, name='account_list'),
    url(r'^organizations/(?P<org_pk>\d+)/accounts/(?P<pk>\d+)$', views.account_detail, name='account_detail'),

    url(r'^organizations/(?P<org_pk>\d+)/plants/$', views.plant_list, name='plant_list'),
    url(r'^organizations/(?P<org_pk>\d+)/plants/(?P<pk>\d+)$', views.plant_detail, name='plant_detail'),

    url(r'^plant_create/$', views.plant_create, name='plant_create'),
    url(r'^plant-upload-sample/$', views.plant_upload_sample, name='plant-upload-sample'),
    url(r'^plant-import/$', views.plant_import, name='plant-import'),
    url(r'^plant-filter/$', views.plant_filter, name='plant-filter'),

    url(r'^release_notes/$', views.release_notes, name='release_notes'),



] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
