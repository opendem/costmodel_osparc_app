import requests
import urllib
import json
from functools import wraps
from pprint import pprint

from costmodel_osparc_app.settings import (
    API_PROTO, API_HOST, API_PORT, API_VERSION
)


API_STORED_SESSIONS = {}


def memoize(func):
    cache = {}
    @wraps(func)
    def wrap(*args, **kwargs):
        key = ''.join([repr(a) for a in args])
        key += ''.join(['{}:{}'.format(k, v) for k, v in kwargs.items()])

        if key not in cache:
            cache[key] = func(*args, **kwargs)
        return cache[key]
    return wrap


class APIResponseException(Exception):
    def __init__(self, method, status_code, path, body, data=None):
        self.method = method
        self.status_code = status_code
        self.body = body
        self.path = path
        self.data = data

    def __str__(self):
        exc_list = []

        exc_list.append('method: {}'.format(self.method))
        exc_list.append('status code: {}'.format(self.status_code))
        exc_list.append('path: {}'.format(self.path))
        exc_list.append('response body: {}'.format(pprint(self.body)))

        if self.data:
            exc_list.append('payload: {}'.format(pprint(self.data)))

        return repr('\n'.join(exc_list))


class APIWrapper(object):

    endpoint = '{proto}://{host}:{port}/{version}/'.format(
        proto=API_PROTO, host=API_HOST, port=API_PORT, version=API_VERSION
    )

    _success_codes = [200, 201, 204]
    _error_codes = [400, 403, 404, 500]

    @classmethod
    def from_session(cls, sessionid):
        if sessionid in API_STORED_SESSIONS:
            return API_STORED_SESSIONS[sessionid]

        raise Exception(
            "APIWrapper instance with given sessionid doesn't exist."
        )

    def __init__(self, email, password):
        self.email = email
        self.password = password

    def make_call(self, method, path, params={}, data={}, headers={}, **kwargs):
        method_params = {}

        http_method = getattr(requests, method)
        default_headers = {'Accept': 'application/json'}

        if headers:
            default_headers.update(headers)

        method_params.update({'headers': default_headers})

        if hasattr(self, 'cookies'):
            method_params.update({'cookies': self.cookies})
            default_headers.update({'X-CSRFToken': self.cookies['csrftoken']})

        if data:
            method_params.update({'data': data})

        if kwargs:
            method_params.update(kwargs)

        url = '{}{}/?{params}'.format(
            self.endpoint, path, params=urllib.urlencode(params)
        )

        response = http_method(url, **method_params)

        result = {'response': response, 'exc': None}

        if response.status_code not in self._success_codes:
            exc = APIResponseException(
                response.request.method, response.status_code,
                response.request.url, response.text, response.request.body,
            )
            result['exc'] = exc

        return result

    def login(self):
        ph = ['login']

        params = {
            'email': self.email, 'password': self.password
        }
        result = self.make_call('get', self.construct_path(ph), params=params)

        response = result['response']
        exception = result['exc']

        if exception:
            raise exception

        data = response.json()

        # failure
        if 'error' in data['status']:
            self.user = None
            return response

        # set attributes after successful login
        for key, val in data.items():
            setattr(self, key.replace('_id', ''), val)

        self.user = data['id']
        self.user_permission = data['user_permission']
        self.failure_analysis_available = data['failure_analysis_available']
        #self.is_superuser = data['is_superuser']
        self.last_login = data['last_login']
        # save copy of cookies with the api wrapper instance
        self.cookies = response.cookies
        self.sessionid = self.cookies.get('sessionid')

        if self.sessionid:
            # store in central repo
            API_STORED_SESSIONS[self.sessionid] = self

        return response

    def _resources(self, method='get', path='', **kwargs):
        failsafe = kwargs.pop('failsafe', False)
        if not hasattr(self, 'organization') or self.organization is None:
            raise Exception('API not logged in.')

        result = self.make_call(method, path, **kwargs)

        exception = result['exc']
        response = result['response']

        if exception and not failsafe:
            raise exception

        return response

    def construct_path(self, hierarchy):
        """Given a list defining hierarchy like
        Organizations > 1 > Plant Groups

        returns a path string like 'organizations/1/plant_groups'
        """
        return '/'.join([str(r) for r in hierarchy])

    def organizations(self, id=None, **kwargs):
        ph = ['organizations']

        if id:
            ph.append(id)

        response = self._resources(path=self.construct_path(ph), **kwargs)
        return response

    def plantgroups(self, id=None, **kwargs):
        ph = ['organizations', self.organization, 'plant_groups']
        if id:
            ph.append(id)

        response = self._resources(path=self.construct_path(ph), **kwargs )
        return response

    def plants(self, id=None, **kwargs):
        ph = ['organizations', self.organization, 'plants']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def plants_list_no_pagination(self, id=None, **kwargs):
        ph = ['organizations', self.organization, 'no-pagination-plants']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def plant_count(self, id=None, **kwargs):
        ph = ['plant', 'count']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def costmodels(self, id=None, **kwargs):
        ph = ['organizations', self.organization, 'cost_models']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def plant_equipments(self, id=None, **kwargs):
        ph = ['plant_equipments']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def plant_finances(self, id=None, **kwargs):
        ph = ['plant_finances']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def servicevalues(self, id=None, **kwargs):
        ph = ['organizations', self.organization, 'service_values']

        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def environmental_conditions(self, id=None, **kwargs):
        ph = ['environmental_conditions']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def weather_sources(self, id=None, **kwargs):
        ph = ['weather_sources']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def pv_market_sectors(self, id=None, **kwargs):
        ph = ['pv_market_sectors']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def inverter_types(self, id=None, **kwargs):
        ph = ['inverter_types']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def module_types(self, id=None, **kwargs):
        ph = ['module_types']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def mounting_locations(self, id=None, **kwargs):
        ph = ['mounting_locations']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def mounting_types(self, id=None, **kwargs):
        ph = ['mounting_types']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def roof_slope_types(self, id=None, **kwargs):
        ph = ['roof_slope_types']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def roof_types(self, id=None, **kwargs):
        ph = ['roof_types']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def inspection_techniques(self, id=None, **kwargs):
        ph = ['inspection_techniques']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def tracking_types(self, id=None, **kwargs):
        ph = ['tracking_types']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def asset_types(self, id=None, **kwargs):
        ph = ['asset_types']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def om_types(self, id=None, **kwargs):
        ph = ['om_types']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def services(self, id=None, **kwargs):
        ph = ['organizations', self.organization, 'services']

        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def service_values(self, id=None, **kwargs):
        ph = ['organizations', self.organization, 'service_values']

        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def plant_time_series(self, id=None, **kwargs):
        ph = ['plants', 'plant_time_series']
        if id:
            ph.insert(1, id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def weather_data(self, id=None, **kwargs):
        ph = ['plants', 'weather_data']
        if id:
            ph.insert(1, id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def geochart(self, id=None, **kwargs):
        ph = ['geochart']

        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def kpis(self, id=None, **kwargs):
        ph = ['kpis', 'calc']

        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def countries(self, id=None, **kwargs):
        ph = ['countries']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)        

    @memoize
    def states(self, id=None, **kwargs):
        ph = ['states']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def cities(self, id=None, **kwargs):
        ph = ['cities']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def kpis_single_plant(self, id=None, **kwargs):
        ph = ['kpis', 'calc']

        if id:
            ph.insert(1, id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def kpis_multiple_plants(self, state_id=None, city_id=None, **kwargs):
        ph = ['kpis', 'multiple-plants-calc']

        if state_id:
            ph.append(state_id)
        if city_id:
            ph.append(city_id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def custom_queries(self, id=None, **kwargs):
        ph = ['kpis', self.organization, 'list']

        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def default_service_values(self, id=None, **kwargs):
        ph = ['organizations', self.organization, 'default-service-values']

        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    @memoize
    def default_costmodel_values(self, id=None, **kwargs):
        ph = ['organizations', self.organization, 'default-costmodel-values']

        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def all_plants_list(self, id=None, **kwargs):
        ph = ['all-plants-list']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def users(self, id=None, **kwargs):
        ph = ['users']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def individual_user(self, id=None, **kwargs):
        ph = ['organizations', self.organization, 'users']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def query_task_status(self, id=None, **kwargs):
        ph = ['query-task-status']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def plant_export(self, id=None, **kwargs):
        ph = ['export']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def user_permissions(self, id=None, **kwargs):
        ph = ['user_permissions']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def plant_groups(self, id=None, **kwargs):
        ph = ['organizations', self.organization, 'plant_groups']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def results(self, id=None, **kwargs):
        ph = ['results']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def plant_group_results(self, id=None, **kwargs):
        ph = ['plant_group_results']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def warranty_types(self, id=None, **kwargs):
        ph = ['warranty_types']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def failure_distribution_types(self, id=None, **kwargs):
        ph = ['failure_distribution_types']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def service_providers(self, id=None, **kwargs):
        ph = ['service_providers']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def applicable_units(self, id=None, **kwargs):
        ph = ['applicable_units']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def asset_types(self, id=None, **kwargs):
        ph = ['asset_types']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def service_types(self, id=None, **kwargs):
        ph = ['service_types']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def file_uploader(self, id=None, **kwargs):
        ph = ['file-upload']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def labor_rates(self, id=None, **kwargs):
        ph = ['organizations', self.organization, 'labor_rates']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def csi_import(self, id=None, **kwargs):
        ph = ['csi-import']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def get_plant_report(self, id=None, **kwargs):
        ph = ['organizations', self.organization, 'plant_reports']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def get_dashboard_stats(self, id=None, **kwargs):
        ph = ['osparc-dashboard-stats']
        if id:
            ph.append(id)

        return self._resources(path=self.construct_path(ph), **kwargs)

    def solar_anywhere_create_weather_data(self, id=None, **kwargs):
        ph = ['plants','solar-anywhere-data']
        if id:
            ph.insert(1, id)

        return self._resources(path=self.construct_path(ph), **kwargs)
