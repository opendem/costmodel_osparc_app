import requests

from django import forms

from constants import BASE_URL


def get_enumerations_from_api():

    enumerations = {}

    environmental_conditions_req = requests.get(BASE_URL + '/environmental_conditions/?format=json')
    enumerations['environmental_conditions'] = environmental_conditions_req.json()

    module_type_req = requests.get(BASE_URL + '/module_types/?format=json')
    enumerations['module_types'] = module_type_req.json()

    market_sector_req = requests.get(BASE_URL + '/pv_market_sectors/?format=json')
    enumerations['market_sectors'] = market_sector_req.json()

    mounting_location_req = requests.get(BASE_URL + '/mounting_locations/?format=json')
    enumerations['mounting_locations'] = mounting_location_req.json()

    mounting_type_req = requests.get(BASE_URL + '/mounting_types/?format=json')
    enumerations['mounting_types'] = mounting_type_req.json()

    roof_slope_req = requests.get(BASE_URL + '/roof_slope_types/?format=json')
    enumerations['roof_slopes'] = roof_slope_req.json()

    roof_type_req = requests.get(BASE_URL + '/roof_types/?format=json')
    enumerations['roof_types'] = roof_type_req.json()

    inspection_tech_req = requests.get(BASE_URL + '/inspection_techniques/?format=json')
    enumerations['inspection_techniques'] = inspection_tech_req.json()

    tracking_req = requests.get(BASE_URL + '/tracking_types/?format=json')
    enumerations['tracking_types'] = tracking_req.json()

    inverter_type_req = requests.get(BASE_URL + '/inverter_types/?format=json')
    enumerations['inverter_types'] = inverter_type_req.json()

    weather_source_req = requests.get(BASE_URL + '/weather_sources/?format=json')
    enumerations['weather_sources'] = weather_source_req.json()

    warranty_type_req = requests.get(BASE_URL + '/warranty_types/?format=json')
    enumerations['warranty_types'] = warranty_type_req.json()

    enumerations['inverter_type_data'] = inverter_type_req.content
    enumerations['module_type_data'] = module_type_req.content

    return enumerations


select_choices = get_enumerations_from_api()


class PlantForm(forms.Form):
    uuid = forms.CharField(max_length=254,
    					   help_text='Universally Unique Plant Identifier')
    name = forms.CharField(max_length=45, required=True)
    description = forms.CharField(max_length=254)
    activation_date = forms.DateField(required=True)

    address_line_1 = forms.CharField(max_length=254, required=False)
    address_line_2 = forms.CharField(max_length=254, required=False)
    city = forms.CharField(max_length=100, required=False)
    county = forms.CharField(max_length=100, required=False)
    state = forms.CharField(max_length=100, required=False)
    country = forms.CharField(max_length=100, required=False)
    postal_code = forms.CharField(max_length=6, required=False)
    latitude = forms.CharField(max_length=16, required=False)
    longitude = forms.CharField(max_length=16, required=False)
    time_zone = forms.CharField(max_length=64, required=False)

    dc_rating = forms.FloatField(required=True,
    							help_text='It is the DC power rating of the PV array'
    							 		' in kilo Watt peak (kWp) at standard test conditions (STC).'
    							 		' The DC Rating determines other plant attributes such as'
    							 		' number of modules string configuration, array area, etc.')
    derate_factor = forms.FloatField(required=True)
    energy_yield = forms.FloatField(required=True,
    								help_text='Energy Yield is the actual energy output of the system'
    								' expressed in kWh/kWp per year.')

    storage_original_capacity = forms.FloatField(required=True)
    storage_current_capacity = forms.FloatField(required=True)
    storage_state_of_charge = forms.FloatField(required=True)
    
    market_sectors = tuple([(choice['id'], choice['name'])
                            for choice in select_choices['market_sectors']])
    pv_market_sector = forms.ChoiceField(choices=market_sectors,
    									required=True)
    env_cond = tuple([(choice['id'], choice['name'])
                      for choice in select_choices['environmental_conditions']])
    environmental_conditions = forms.MultipleChoiceField(
        choices=env_cond,
        required=False,
        widget=forms.CheckboxSelectMultiple,
        help_text='The conditions in the environment surrounding the PV System.'
    )

    weather_source = forms.ChoiceField(choices=(), required=False)
    plant_finance = forms.IntegerField(required=True, widget=forms.HiddenInput())
    plant_equipment = forms.IntegerField(required=True, widget=forms.HiddenInput())
    active_cost_model = forms.IntegerField()


class PlantEquipmentForm(forms.Form):
    inverter_types = tuple([(choice['id'], choice['name'])
                            for choice in select_choices['inverter_types']])
    inverter_type = forms.ChoiceField(choices=inverter_types,
     								  required=True,
     								  help_text='The category of Inverter with regards to size'
     								  			' and electrical interface type.')
    inverter_replacement_cost = forms.FloatField(help_text='The cost of replacement of a PV'
    													  ' System Inverter in the future.'
    											          ' This cost is stated in U.S. dollars per DC watt peak.')
    inverter_capacity = forms.FloatField(required=True,
    									help_text='The amount of load that the inverter can handle;'
    											  ' expressed in kWp,')
    number_of_inverters = forms.IntegerField(required=True)

    number_of_transformers = forms.IntegerField(required=True)
    module_types = tuple([(choice['id'], choice['name'])
                            for choice in select_choices['module_types']])
    module_type = forms.ChoiceField(choices=module_types,
     								required=True,
     								help_text='The category of PV Module with regard to physical composition and chemistry.')
    module_degradation_rate = forms.FloatField(help_text='The assumed rate efficiency loss for'
    										             ' the PV Modules used in the PV System.')
    balance_of_system_degradation_rate = forms.FloatField()  # per year

    module_efficiency = forms.FloatField(required=True,
    									help_text='Module Efficiency is the percentage of'
    									          ' solar energy that is converted into electricity')
    module_power = forms.FloatField(required=True,
    								help_text='Module Power is the Power Rating of the PV'
    								          ' module in Watts under standard test conditions (STC).')
    modules_per_row = forms.IntegerField()
    modules_per_string = forms.IntegerField()
    mounting_types = tuple([(choice['id'], choice['name'])
                            for choice in select_choices['mounting_types']])
    mounting_type = forms.ChoiceField(choices=mounting_types,
    								  required=True,
    								  help_text='The category of mechanical apparatus used to fix'
    								  			' PV Modules to the Mounting Location.')
    mounting_locations = tuple([(choice['id'], choice['name'])
                            for choice in select_choices['mounting_locations']])
    mounting_location = forms.ChoiceField(choices=mounting_locations,
     									  required=True,
     									  help_text='The surface upon which the Mounting'
     									  ' Structure of the PV System is installed.')
    roof_slopes = tuple([(choice['id'], choice['name'])
                            for choice in select_choices['roof_slopes']])
    roof_slope_type = forms.ChoiceField(choices=roof_slopes,
     									required=True,
     									help_text='The category of Roof Slope with regard'
     									' to the ratio of the vertical'
                                    	' rise to the horizontal span of the roof.')
    roof_types = tuple([(choice['id'], choice['name'])
                            for choice in select_choices['roof_types']])
    roof_type = forms.ChoiceField(choices=roof_types,
     							  required=True,
    							  help_text='The category of Roof with regard to material type.')
    inspection_techniques = tuple([(choice['id'], choice['name'])
                            for choice in select_choices['inspection_techniques']])
    inspection_technique = forms.ChoiceField(choices=inspection_techniques,
     										required=True,
     										help_text='The category of observation location.')
    tracking_types = tuple([(choice['id'], choice['name'])
                            for choice in select_choices['tracking_types']])
    tracking = forms.ChoiceField(choices=tracking_types,
     							required=True,
     							help_text='The category of mechanical apparatus that'
     									  ' orients PV Modules toward the sun.')
    array_area_per_roof_attachment = forms.FloatField()
    combiner_boxes_per_dcd = forms.IntegerField()
    foundations_per_row = forms.IntegerField()
    gcr = forms.FloatField(required=True)

    rows_per_tracked_block = forms.IntegerField()
    strings_per_combiner_box = forms.IntegerField()
    tilt = forms.IntegerField(required=True)
    azimuth = forms.IntegerField(required=True)
    pfid = forms.IntegerField(required=True, widget=forms.HiddenInput())


class PlantFinanceForm(forms.Form):
    system_installed_cost = forms.FloatField(required=True)

    analysis_period = forms.IntegerField(required=True,
    									help_text='Analysis Period is the time period'
    									          ' for which the cash flow analysis is being done.')
    discount_rate = forms.FloatField(required=True,
    								help_text='The Discount Rate is a measure of the time'
    								          ' value of money expressed as an annual rate.')
    inflation_rate = forms.FloatField(required=True,
    								  help_text='Inflation rate is the rate of cost change'
    								            ' per annum. Inflation rate is typically based on price index.')
    desired_confidence_that_reserve_covers_cost = forms.FloatField(required=True,
    									help_text='The desired confidence ratio that the Reserve Account'
    									          ' would be sufficient to cover the cost of a failure.'
    									          ' For Example: 99.9% would be represented as 0.999')

    epc_warranty = forms.IntegerField(help_text='The warranty provided by the EPC covering'
    										  ' the construction of the PV system.')
    inverter_warranty = forms.IntegerField(help_text='The warranty provided by the Inverter Manufacturer.')
    module_warranty = forms.IntegerField(help_text='The warranty provided by the PV Module Manufacturer.')
    monitoring_warranty = forms.IntegerField(help_text='The warranty provided by the Monitoring Provider.')

    working_hours = forms.IntegerField()


class OrganizationForm(forms.Form):
    name = forms.CharField(max_length=45)
    description = forms.CharField(max_length=254, required=False)
    cost_model_license = forms.BooleanField()
    osparc_license = forms.BooleanField()

class FileUploaderForm(forms.Form):
    time_series = forms.FileField()
    plant_create = forms.FileField()