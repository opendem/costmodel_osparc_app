from functools import wraps

from django.contrib.auth.models import AnonymousUser
from django.shortcuts import render, redirect
from django.contrib import messages
from django.core.mail import send_mail
from django.conf import settings
from django.urls import reverse
from django.http import Http404, HttpResponse, JsonResponse
from django.core.exceptions import (
    MultipleObjectsReturned, ObjectDoesNotExist
)

from core.forms import (
    PlantForm, PlantEquipmentForm, PlantFinanceForm,
    OrganizationForm, FileUploaderForm
)
from constants import BASE_URL
from constants import countries
from constants import states_dict
from constants import cities

import requests
import json, csv
import urllib
import cgi
from dateparser import parse
from openpyxl import load_workbook
import datetime

from .api import APIWrapper, APIResponseException
from costmodel_osparc_app.settings import STATIC_ROOT


def login_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        login_context = {}
        try:
            login_context['org_id'] = args[0].session['org_id']
            login_context['org_name'] = args[0].session['org_name']
            login_context['user_id'] = args[0].session['user_id']
            login_context['sessionid'] = args[0].session['sessionid']
            login_context['user_permission'] = args[0].session['user_permission']
            login_context['cost_model_license'] = args[0].session['cost_model_license']
            login_context['osparc_license'] = args[0].session['osparc_license']
            login_context['is_superuser'] = args[0].session['is_superuser']
            login_context['failure_analysis_available'] = args[0].session['failure_analysis_available']

            kwargs['login_context'] = login_context

        except KeyError:
            return redirect('auth')

        return f(*args, **kwargs)
    return wrapper


def index(request):
    try:
        org_id = request.session['org_id']
        org_name = request.session['org_name']
        sessionid = request.session['sessionid']
        user_permission = request.session['user_permission']
        cost_model_license = request.session['cost_model_license']
        osparc_license = request.session['osparc_license']
    except KeyError:
        return redirect('auth')
    cookies = {'sessionid': sessionid}

    api = APIWrapper.from_session(sessionid)
    is_superuser = api.is_superuser

    # plant_groups
    pg_count = 0
    cm_count = 0
    sv_count = 0
    if cost_model_license:
        response = api.plantgroups()
        pg_count = len(response.json())  # len on list of plant groups

        # costmodels
        if org_id != 1:
            response = api.default_costmodel_values()
            cm_count = len(response.json())  # len on list of costmodels
        else:
            response = api.costmodels()
            cm_count = len(response.json())

        # service values
        if org_id != 1:
            response = api.default_service_values()
            sv_count = len(response.json())  # len on list of plants
            response = api.servicevalues()
            sv_count_one = len(response.json())
            sv_count = sv_count_one + sv_count
        else:
            response = api.servicevalues()
            sv_count = len(response.json())

    if org_id == 1:
        p_count = api.plants().json()['count']
    else:
        p_count = api.plants().json()['count']

    return render(request, 'costmodel/index.html',
                  {
                      'org_name': org_name, 'plant_group_count': pg_count,
                      'plant_count': p_count, 'costmodel_count': cm_count,
                      'service_count': sv_count, 'org_id': org_id, 'user_permission': user_permission,
                      'cost_model_license': cost_model_license, 'osparc_license': osparc_license,
                      'last_login': api.last_login, 'is_superuser': is_superuser
                  })


def help_view(request):
    try:
        org_id = request.session['org_id']
        user_permission = request.session['user_permission']
        cost_model_license = request.session['cost_model_license']
        osparc_license = request.session['osparc_license']
    except KeyError:
        return redirect('auth')
    return render(request, 'core/help.html',
                  {
                      'org_id': org_id, 'user_permission': user_permission,
                      'cost_model_license': cost_model_license, 'osparc_license': osparc_license
                  })


def logout(request):
    requests.get(BASE_URL+'/logout/?format=json')
    request.session.flush()
    if hasattr(request, 'user'):
        request.user = AnonymousUser()
    return redirect('auth')


def login_anywhere(request, email, password):
    api = APIWrapper(email=email, password=password)
    login_response = api.login()

    if not api.user:
        return

    # populate session with csrftoken and sessionid
    # from api
    request.session.set_expiry(0)
    request.session['sessionid'] = login_response.cookies['sessionid']
    request.session['csrftoken'] = login_response.cookies['csrftoken']

    # populate session with organization and user details
    request.session['user_id'] = api.user
    request.session['org_id'] = api.organization
    request.session['failure_analysis_available'] = api.failure_analysis_available
    request.session['cost_model_license'] = api.cost_model_license
    request.session['osparc_license'] = api.osparc_license
    request.session['user_permission'] = api.user_permission
    request.session['is_superuser'] = api.is_superuser

    organization_detail_response = api.organizations(id=api.organization)

    org_details = organization_detail_response.json()
    request.session['org_name'] = org_details['name']

    return api


def auth(request):
    next_ = request.GET.get('next', '')

    if request.method == 'POST' and 'loginbtn' in request.POST:
        email = request.POST.get('email')
        password = request.POST.get('password')
        next_ = request.POST.get('redirect_to_next')
        api = login_anywhere(request, email, password)

        if not api:
            messages.warning(request, 'Invalid credentials. Please try again.')
            return redirect('auth')

        if next_:
            return redirect(next_)
        if not api.cost_model_license and api.osparc_license:
            return redirect('dashboard')
        return redirect('index')

    parsed_data = []

    if request.method == 'POST' and 'registerbtn' in request.POST:
        email = request.POST.get('email')
        password = request.POST.get('password')
        verify_password = request.POST.get('verify_password')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        org_id = request.POST.get('org_id')

        if password == verify_password:
            req = requests.get(
                BASE_URL + '/register/?email=' + email + '&password=' + password + '&first_name=' + first_name +
                '&last_name=' + last_name + '&org_id=' + org_id)
            jsonList = []
            jsonList.append(json.loads(req.content))
            userData = {}

            for data in jsonList:
                userData['status'] = data['status']
            parsed_data.append(userData)

            if userData['status'] == "success":
                messages.success(request, "Successfully Registered!")
                return redirect('index')
            else:
                messages.warning(request, "Error! User already exists")
        else:
            messages.warning(request, "Error! Passwords do not match")

    elif request.method == 'POST' and 'forgot_password_btn' in request.POST:
        email = request.POST.get('email')
        req = requests.get(BASE_URL + '/forgot-password/?email='+email+'&format=json')
        if req.status_code == 200:
            data = req.json()
            if data['status'] == 'success':
                messages.success(request, "Success, You will receive an email with a link to reset your password.")
            else:
                messages.warning(request, data['status'])
        else:
            messages.warning(request, "No email was found matching your request, please try again.")

    return render(request, 'core/auth.html', {'next': next_})


def reset_password_confirm(request, uidb64, token):
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('confirm_password')
        data = {
            'email': email,
            'password': password,
            'uidb64': uidb64,
            'token': token,
        }
        req = requests.get(
            BASE_URL+'/forgot-password-complete/?email='+email+'&uidb64='+uidb64+'&token='+token+'&password='+password+'&format=json'
        )
        data = req.json()
        if data['status'] == 'success':
            messages.success(request, "Your password was successfully reset, you may now login.")
            return redirect('auth')
        else:
            messages.warning(request, data['status'])

    return render(request, 'core/reset_password_confirm.html', {})

@login_required
def profile(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')

    api = APIWrapper.from_session(login_context['sessionid'])
    cookies = {'sessionid': login_context['sessionid']}
    jsonList = []
    user_id = login_context['user_id']
    req = requests.get(BASE_URL+'/organizations/'+str(login_context['org_id'])+'/users/'+str(user_id)+'/?depth=2&format=json', cookies=cookies)
    jsonList.append(json.loads(req.content))
    parsedData = []
    userData = {}
    for data in jsonList:
        userData['email'] = data['email']
        userData['first_name'] = data['first_name']
        userData['last_name'] = data['last_name']
        userData['user_permission'] = data['user_permission']['name']
    parsedData.append(userData)

    login_context.update({'data': parsedData})

    if request.method == 'POST' and 'reset_btn' in request.POST:
        email = request.POST.get('email')
        password = request.POST.get('password')
        new_password = request.POST.get('new_password')
        req = requests.get(BASE_URL + '/reset_password/?email='+email+'&password='+password+'&new_password='+new_password+'&format=json')
        data = req.json()
        if data['status'] == 'success':
            messages.success(request, "Success! Password has been changed")
        else:
            messages.warning(request, data['status'])
        api = login_anywhere(request, email, new_password)

        if not api:
            messages.warning(request, 'Invalid credentials. Please try again.')
            return redirect('auth')

        def wrap(request, **kwargs):
            return request, kwargs

        lwrapped = login_required(wrap)

        _, kwargs = lwrapped(request)
        login_context.update(kwargs['login_context'])

    return render(request, 'core/profile.html', login_context)

@login_required
def plant_list(request, org_pk, *args, **kwargs):
    login_context = kwargs.pop('login_context')

    api = APIWrapper.from_session(login_context['sessionid'])

    if 'GET' in request.method:
        login_context.update({
            'pv_market_sectors': api.pv_market_sectors().json()
        })

        return render(request, 'core/plant_list.html', login_context)

    if request.is_ajax():
        start = request.POST.get('start')

        data = {}
        params = {'offset': start, 'limit': 10}

        filter_by_keys = [
            k for k in request.POST.keys() 
            if not k.startswith('column') and
            not k.startswith('order') and
            not k.startswith('csrf') and
            not k.startswith('search') and
            not k.startswith('length') and
            not k.startswith('draw') and
            not k.startswith('start')
        ]

        for key in filter_by_keys:
            params[key] = request.POST[key]

        response = api.plants(params=params)
        data['data'] = response.json()['results']
        for d in data['data']:
            if d['state']:
                state = api.states(id=d['state']).json()
                d.update({'state': state['name']})
            if d['city']:
                city = api.cities(id=d['city']).json()
                d.update({'city': city['name']})
            if d['pv_market_sector']:
                market_sector = api.pv_market_sectors(id=d['pv_market_sector']).json()
                d.update({'pv_market_sector': market_sector['name']})

        data.update({
            "options": [],
            "files": [],
            "draw": request.POST.get('draw'),
            "recordsTotal": response.json()['count'],
            "recordsFiltered": response.json()['count']
            })
        return JsonResponse(data, safe=False)

    if request.method == 'POST' and 'plant_filter_btn' in request.POST:
        count = 1
        _continue = True
        filters = {}
        p = lambda x: '{}{}'.format(
            request.POST['attribute_%d' % x],
            request.POST['comparison_attribute_%d' % x]
        )

        while _continue:
            try:
                filters[p(count)] = request.POST['value_attribute_%d' % count]
                count += 1
            except KeyError:
                _continue = False

        filters.update(
            {
                'city': request.POST.get('city', ''),
                'state': request.POST.get('state', ''),
            }
        )

        plants = api.plants(params=filters)
        plant_list_data = plants.json()['results']

        for data in plant_list_data:
            if data['state']:
                state = api.states(id=data['state']).json()
                data.update({'state': state['name']})
            if data['city']:
                city = api.cities(id=data['city']).json()
                data.update({'city': city['name']})

        login_context.update({
            'filter_data': filters,
            'pv_market_sectors': api.pv_market_sectors().json()
        })

        if not plant_list_data:
            messages.warning(request, "No plants match your selection criteria. Please try a new query.")
            return redirect('plant-filter')
        return render(request, 'core/plant_list.html', login_context)

    return render(request, 'core/plant_list.html', login_context)


@login_required
def plant_create(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid'])

    login_context.update({
        'environmental_conditions': api.environmental_conditions().json(),
        'weather_sources': api.weather_sources().json(),
        'pv_market_sectors': api.pv_market_sectors().json(),
        'inverter_types': api.inverter_types().json(),
        'inverter_type_data': api.inverter_types().content,
        'module_types': api.module_types().json(),
        'module_type_data': api.module_types().content,
        'inspection_techniques': api.inspection_techniques().json(),
        'mounting_locations': api.mounting_locations().json(),
        'mounting_types': api.mounting_types().json(),
        'roof_slope_types': api.roof_slope_types().json(),
        'roof_types': api.roof_types().json(),
        'tracking_types': api.tracking_types().json(),
        'create_plant': True,
        'cost_models': api.costmodels(failsafe=True).json(),
        'plant_detail': {'environmental_condition': [], 'cost_model': []},  # placeholder
        'states': states_dict[231],  # hardcoding US states
        'states_dict': json.dumps(states_dict),
        'cities': json.dumps(cities),
        'cities_dict': cities,
        'country': countries,
    })

    if 'GET' in request.method:
        return render(request, 'core/plant_detail.html', login_context)

    if 'POST' in request.method:
        if 'plant_import_btn' in request.POST:
            file_name = STATIC_ROOT + '/core/example_files/PlantUploadTemplate.csv'
            file = open(file_name, 'rb')
            read_template = csv.reader(file)
            all_data = list(read_template)

            datafile = request.FILES['datafile']
            plants_data = []
            for plant in datafile.readlines():
                plant = plant.strip().split(',')
                plants_data.append(plant)
            Reader = csv.reader(datafile)
            Data = list(Reader)

            template_data = all_data[0]
            user_data = plants_data[0]

            if template_data != user_data:
                messages.warning(request, "It looks like the csv that you uploaded does not match the template. Please download the template and follow exact format of examples.")
                return redirect('plant_create')

            # all the code below is validation of the imported csv file

            try:
                state = api.states(params={'name': Data[1][7], 'country': 231})
                state = state.json()
                if state:
                    state = state[0]['id']
            except ObjectDoesNotExist:
                state = None

            try:
                city = api.cities(params={'name': Data[1][5]}).json()
                if city:
                    city = city[0]['id']
            except ObjectDoesNotExist:
                city = None

            try:
                market_sector = api.pv_market_sectors(params={'name': Data[1][19]}).json()[0]['id']
            except ObjectDoesNotExist:
                market_sector = None

            try:
                all_conditions = []
                if ',' in Data[1][20]:
                    for d in Data[1][20].split(","):
                        env_cond_req = api.environmental_conditions(params={'name': d})
                        all_conditions.append(env_cond_req.json()[0]['id'])
                else:
                    all_conditions.append(api.environmental_conditions(params={'name': Data[1][20]}).json()[0]['id'])
            except ObjectDoesNotExist:
                all_conditions = []

            required_fields = [Data[1][0], Data[1][2], Data[1][13], Data[1][15]]

            if not all(required_fields):
                messages.warning(request, "It looks like your missing required fields. Please view example again and follow exact format of examples.")
                return redirect('plant_create')

            try:
                date=parse(
                    Data[1][2],
                    settings={'DATE_ORDER': 'YMD'}).strftime('%Y-%m-%d')
            except:
                messages.warning(request, "It looks like your date format is not correct in column C, please enter the correct format (YYYY-MM-DD) and try again.")
                return redirect('plant_create')

            try:
                #convert dc rating to float
                dc_rating = float(Data[1][13])
            except:
                messages.warning(request, "The value for DC Rating must be a number, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][14]:
                    derate_factor = None
                else:
                    derate_factor = float(Data[1][14])
            except:
                messages.warning(request, "The value for Derate Factor must be a number, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                #convert energy_yield to float
                energy_yield = float(Data[1][15])
            except:
                messages.warning(request, "The value for Energy Yield is required and must be a number, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][16]:
                    storage_original = None
                else:
                    storage_original = float(Data[1][16])
            except:
                messages.warning(request, "The values for Storage Original Capacity must be a number, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][17]:
                    storage_current = None
                else:
                    storage_current = float(Data[1][17])
            except:
                messages.warning(request, "The value for Storage Current Capacity must be a number, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][18]:
                    storage_state = None
                else:
                    storage_state = float(Data[1][18])
            except:
                messages.warning(request, "The value for Storage State of Charge must be a number, please try again after you update your spreadsheet.")
                return redirect('plant_create')                                

            plant_data = {
                    'name': Data[1][0],
                    'description': Data[1][1],
                    'activation_date': date,
                    'address_line_1': Data[1][3],
                    'address_line_2': Data[1][4],
                    'city': city,
                    'county': Data[1][6],
                    'state': state,
                    'country': countries[0]['id'],
                    'postal_code': Data[1][9],
                    'latitude': Data[1][10],
                    'longitude': Data[1][11],
                    'time_zone': Data[1][12],
                    'dc_rating': dc_rating,
                    'derate_factor': derate_factor,
                    'energy_yield': energy_yield,
                    'storage_original_capacity': storage_original,
                    'storage_current_capacity': storage_current,
                    'storage_state_of_charge': storage_state,
                    'pv_market_sector': market_sector,
                    'environmental_condition': all_conditions,
                    'organization': login_context['org_id']
            }

            try:
                if not Data[1][21]:
                    system_installed_cost = None
                else:
                    system_installed_cost = float(Data[1][21])
            except:
                messages.warning(request, "The value for System Installed Cost must be a number, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][22]:
                    analysis_period = None
                else:
                    analysis_period = int(Data[1][22])
            except:
                messages.warning(request, "The value for Analysis Period must be a number, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][23]:
                    discount_rate = None
                else:
                    discount_rate = float(Data[1][23])
            except:
                messages.warning(request, "The value for Discount Rate must be a number, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][24]:
                    inflation_rate = None
                else:
                    inflation_rate = float(Data[1][24])
            except:
                messages.warning(request, "The value for Inflation Rate must be a number, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][25]:
                    desired_confidence_that_reserve_covers_cost = None
                else:
                    desired_confidence_that_reserve_covers_cost = float(Data[1][25])
            except:
                messages.warning(request, "The value for Desired Confidence that Reserve Covers Cost must be a number, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][26]:
                    epc_warranty = None
                else:
                    epc_warranty = int(Data[1][26])
            except:
                messages.warning(request, "The value for EPC Warranty must be a number with no decimals, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][27]:
                    inverter_warranty = None
                else:
                    inverter_warranty = int(Data[1][27])
            except:
                messages.warning(request, "The value for Inverter Warranty must be a number with no decimals, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][28]:
                    module_warranty = None
                else:
                    module_warranty = int(Data[1][28])
            except:
                messages.warning(request, "The value for Module Warranty must be a number with no decimals, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][29]:
                    monitoring_warranty = None
                else:
                    monitoring_warranty = int(Data[1][29])
            except:
                messages.warning(request, "The value for Monitoring Warranty must be a number with no decimals, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][30]:
                    working_hours = None
                else:
                    working_hours = int(Data[1][30])
            except:
                messages.warning(request, "The value for Working Hours must be a number with no decimals, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            # Plant Finance Create if data exists
            if any(Data[1][21:30]):
                pf_data = {
                    'system_installed_cost': system_installed_cost,
                    'analysis_period': analysis_period,
                    'discount_rate': discount_rate,
                    'inflation_rate': inflation_rate,
                    'desired_confidence_that_reserve_covers_cost': desired_confidence_that_reserve_covers_cost,
                    'epc_warranty': epc_warranty,
                    'inverter_warranty': inverter_warranty,
                    'module_warranty': module_warranty,
                    'monitoring_warranty': monitoring_warranty,
                    'working_hours': working_hours,
                }
                pf_response = api.plant_finances(method='post', data=pf_data)
                if pf_response.status_code == 201:
                    pf_response = api.plant_finances(method='post', data=pf_data).json()
                    plant_data['plant_finance'] = pf_response['id']

            # Plant Equipment Create if data exists
            try:
                tracking = api.tracking_types(params={'name': Data[1][48]}).json()
                if tracking:
                    tracking = tracking[0]['id']
            except ObjectDoesNotExist:
                tracking = None

            try:
                inverter_types = api.inverter_types(params={'name': Data[1][31]}).json()
                if inverter_types:
                    inverter_types = inverter_types[0]['id']
            except ObjectDoesNotExist:
                inverter_types = None

            try:
                module_types = api.module_types(params={'name': Data[1][36]}).json()
                if module_types:
                    module_types = module_types[0]['id']
            except ObjectDoesNotExist:
                module_types = None

            try:
                mounting_location = api.mounting_locations(params={'name': Data[1][44]}).json()
                if mounting_location:
                    mounting_location = mounting_location[0]['id']
            except ObjectDoesNotExist:
                mounting_location = None

            try:
                mounting_types = api.mounting_types(params={'name': Data[1][43]}).json()
                if mounting_types:
                    mounting_types = mounting_types[0]['id']
            except ObjectDoesNotExist:
                mounting_types = None

            try:
                roof_slope_types = api.roof_slope_types(params={'name': Data[1][45]}).json()
                if roof_slope_types:
                    roof_slope_types = roof_slope_types[0]['id']
            except ObjectDoesNotExist:
                roof_slope_types = None

            try:
                roof_types = api.roof_types(params={'name': Data[1][46]}).json()
                if roof_types:
                    roof_types = roof_types[0]['id']
            except ObjectDoesNotExist:
                roof_types = None

            try:
                inspection_techniques = api.inspection_techniques(params={'name': Data[1][47]}).json()
                if inspection_techniques:
                    inspection_techniques = inspection_techniques[0]['id']
            except ObjectDoesNotExist:
                inspection_techniques = None

            try:
                if not Data[1][32]:
                    inverter_replacement_cost = None
                else:
                    inverter_replacement_cost = float(Data[1][32])
            except:
                messages.warning(request, "The value for Inverter Replacement Cost must be a number, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][33]:
                    inverter_capacity = None
                else:
                    inverter_capacity = float(Data[1][33])
            except:
                messages.warning(request, "The value for Inverter Capacity must be a number, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][34]:
                    number_of_inverters = None
                else:
                    number_of_inverters = int(Data[1][34])
            except:
                messages.warning(request, "The value for Number of Inverters must be a number with no decimals, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            try:
                if not Data[1][35]:
                    number_of_transformers = None
                else:
                    number_of_transformers = int(Data[1][35])
            except:
                messages.warning(request, "The value for Number of Transformers must be a number with no decimals, please try again after you update your spreadsheet.")
                return redirect('plant_create')

            if Data[1][37]:
                try:
                    value = float(Data[1][37])
                    Data[1].pop(37)
                    Data[1].insert(37, value)
                except:
                    messages.warning(request, "The value for Module Degradation Rate must be a number, please try again after you update your spreadsheet.")
                    return redirect('plant_create')

            if Data[1][38]:
                try:
                    value = float(Data[1][38])
                    Data[1].pop(38)
                    Data[1].insert(38, value)
                except:
                    messages.warning(request, "The value for Balance of System Degradation Rate must be a number, please try again after you update your spreadsheet.")
                    return redirect('plant_create')

            if Data[1][39]:
                try:
                    value = float(Data[1][39])
                    Data[1].pop(39)
                    Data[1].insert(39, value)
                except:
                    messages.warning(request, "The value for Module Efficiency must be a number, please try again after you update your spreadsheet.")
                    return redirect('plant_create')

            if Data[1][40]:
                try:
                    value = float(Data[1][40])
                    Data[1].pop(40)
                    Data[1].insert(40, value)
                except:
                    messages.warning(request, "The value for Module Power must be a number with no decimals, please try again after you update your spreadsheet.")
                    return redirect('plant_create')

            if Data[1][41]:
                try:
                    value = int(Data[1][41])
                    Data[1].pop(41)
                    Data[1].insert(41, value)
                except:
                    messages.warning(request, "The value for Modules per row must be a number with no decimals, please try again after you update your spreadsheet.")
                    return redirect('plant_create')

            if Data[1][42]:
                try:
                    value = int(Data[1][42])
                    Data[1].pop(42)
                    Data[1].insert(42, value)
                except:
                    messages.warning(request, "The value for Modules per string must be a number with no decimals, please try again after you update your spreadsheet.")
                    return redirect('plant_create')

            if Data[1][49]:
                try:
                    value = float(Data[1][49])
                    Data[1].pop(49)
                    Data[1].insert(49, value)
                except:
                    messages.warning(request, "The value for Array Area per Roof Attachment must be a number, please try again after you update your spreadsheet.")
                    return redirect('plant_create')

            if Data[1][50]:
                try:
                    value = int(Data[1][50])
                    Data[1].pop(50)
                    Data[1].insert(50, value)
                except:
                    messages.warning(request, "The value for Combiner Boxes per DCD must be a number with no decimals, please try again after you update your spreadsheet.")
                    return redirect('plant_create')

            if Data[1][51]:
                try:
                    value = int(Data[1][51])
                    Data[1].pop(51)
                    Data[1].insert(51, value)
                except:
                    messages.warning(request, "The value for Foundations per row must be a number with no decimals, please try again after you update your spreadsheet.")
                    return redirect('plant_create')

            if Data[1][52]:
                try:
                    value = float(Data[1][52])
                    Data[1].pop(52)
                    Data[1].insert(52, value)
                except:
                    messages.warning(request, "The value for Ground Coverage Ratio must be a number, please try again after you update your spreadsheet.")
                    return redirect('plant_create')

            if Data[1][53]:
                try:
                    value = int(Data[1][53])
                    Data[1].pop(53)
                    Data[1].insert(53, value)
                except:
                    messages.warning(request, "The value for Rows per Tracked Block must be a number with no decimals, please try again after you update your spreadsheet.")
                    return redirect('plant_create')

            if Data[1][54]:
                try:
                    value = int(Data[1][54])
                    Data[1].pop(54)
                    Data[1].insert(54, value)
                except:
                    messages.warning(request, "The value for Strings per Combiner Box must be a number with no decimals, please try again after you update your spreadsheet.")
                    return redirect('plant_create')

            if Data[1][55]:
                try:
                    value = int(Data[1][55])
                    if value <= 360:
                        Data[1].pop(55)
                        Data[1].insert(55, value)
                    else:
                        messages.warning(request, "The value for Tilt must be a number with no decimals and less than 360, please try again after you update your spreadsheet.")
                        return redirect('plant_create')
                except:
                    messages.warning(request, "The value for Tilt must be a number with no decimals and less than 360, please try again after you update your spreadsheet.")
                    return redirect('plant_create')

            if Data[1][56]:
                try:
                    value = int(Data[1][56])
                    if value <= 360:
                        Data[1].pop(56)
                        Data[1].insert(56, value)
                    else:
                        messages.warning(request, "The value for Azimuth must be a number with no decimals and less than 360, please try again after you update your spreadsheet.")
                        return redirect('plant_create')
                except:
                    messages.warning(request, "The value for Azimuth must be a number with no decimals and less than 360, please try again after you update your spreadsheet.")
                    return redirect('plant_create')

            if any(Data[1][31:56]):
                pe_data = {
                    'inverter_type': inverter_types,
                    'inverter_replacement_cost': inverter_replacement_cost,
                    'inverter_capacity': inverter_capacity,
                    'number_of_inverters': number_of_inverters,
                    'number_of_transformers': number_of_transformers,
                    'module_type': module_types,
                    'module_degradation_rate': Data[1][37],
                    'balance_of_system_degradation_rate': Data[1][38],
                    'module_efficiency': Data[1][39],
                    'module_power': Data[1][40],
                    'modules_per_row': Data[1][41],
                    'modules_per_string': Data[1][42],
                    'mounting_type': mounting_types,
                    'mounting_location': mounting_location,
                    'roof_slope_type': roof_slope_types,
                    'roof_type': roof_types,
                    'inspection_technique': inspection_techniques,
                    'tracking': tracking,
                    'array_area_per_roof_attachment': Data[1][49],
                    'combiner_boxes_per_dcd': Data[1][50],
                    'foundations_per_row': Data[1][51],
                    'gcr': Data[1][52],
                    'rows_per_tracked_block': Data[1][53],
                    'strings_per_combiner_box': Data[1][54],
                    'tilt': Data[1][55],
                    'azimuth': Data[1][56],
                }
                pe_response = api.plant_equipments(method='post', data=pe_data)
                if pe_response.status_code == 201:
                    pe_response = api.plant_equipments(method='post', data=pe_data).json()
                    plant_data['plant_equipment'] = pe_response['id']            

            plant_create_res = api.plants(method='post', data=plant_data)
            if plant_create_res.status_code == 201:
                plant_create = plant_create_res.json()
                messages.success(request, 'Plant Successfully Created.')
                return redirect('plant_detail', org_pk=login_context['org_id'], pk=plant_create['id'])
            else:
                messages.warning(request, "It looks like your csv values aren't correct. Please view example again and follow exact format of examples.")
                return redirect('plant_create')
            
        # create plant finance
        pf = PlantFinanceForm(request.POST)
        pf.is_valid()
        pfdata = pf.cleaned_data

        pf_response = api.plant_finances(method='post', data=pfdata).json()

        # create plant equipment
        pe = PlantEquipmentForm(request.POST)
        pe.is_valid()
        pedata = pe.cleaned_data

        pe_response = api.plant_equipments(method='post', data=pedata).json()

        # create plant
        pg = PlantForm(request.POST)

        pg.is_valid()
        pgdata = pg.cleaned_data

        # need to update dc_rating to be stored as watts
        pgdata.update({'dc_rating': pgdata['dc_rating']*1000})

        pgdata['activation_date'] = request.POST.get('activation_date')
        pgdata['plant_equipment'] = pe_response['id']
        pgdata['plant_finance'] = pf_response['id']
        pgdata['organization'] = login_context['org_id']

        # to get lat and long from a user inputted address
        if (
            pgdata['address_line_1'] and 
            pgdata['state'] and 
            pgdata['city'] and not 
            pgdata['latitude'] and not pgdata['longitude']
            ):
            google_api_key = 'AIzaSyAW1BYBGbfPXqiO31ulqJF7RM5ZyNcJEh8'
            timezone_api_key = 'AIzaSyDcHxGNdHyrxahy9N_CokSMuYTBJUGh4X4'
            state = api.states(id=pgdata['state']).json()
            city = api.cities(id=pgdata['city']).json()
            street_address = pgdata['address_line_1']
            data = requests.get(
                'https://maps.googleapis.com/maps/api/geocode/json?address={},+{},+{}&key={}'.format(
                    street_address,
                    city['name'],
                    state['name'],
                    google_api_key,
                )
            )
            data = data.json()
            if data['status'] == 'OK':
                latitude = data['results'][0]['geometry']['location']['lat']
                longitude = data['results'][0]['geometry']['location']['lng']
                county = data['results'][0]['address_components'][3]['long_name']
                postal_code = data['results'][0]['address_components'][6]['long_name']
                
                pgdata.update({
                    'latitude': latitude,
                    'longitude': longitude,
                    'county': county,
                    'postal_code': postal_code,
                })

                #populate timezone from google api based on lat, long
                get_timezone = requests.get(
                    'https://maps.googleapis.com/maps/api/timezone/json?location={},{}&timestamp=1331161200&key={}'.format(
                        latitude,
                        longitude,
                        timezone_api_key,
                    )
                )
                get_timezone = get_timezone.json()
                if get_timezone['status'] == 'OK':
                    timezone = get_timezone['timeZoneId']
                    pgdata.update({'time_zone': timezone})
        
        pg_response = api.plants(method='post', data=pgdata).json()
        messages.success(request, 'Plant successfully created.')

        return redirect('plant_detail', org_pk=login_context['org_id'], pk=pg_response['id'])


def plant_upload_sample(request):
    filename = 'PlantUploadTemplate.csv'
    file_location = STATIC_ROOT + '/core/example_files/PlantUploadTemplate.csv'
    csv_file = open(file_location, 'r+')
    response = HttpResponse(csv_file, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename

    return response


@login_required
def plant_import(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid'])

    form = FileUploaderForm()
    login_context.update({'form': form})
    if request.method == 'POST':
        if 'plant_create_btn' in request.POST:
            plant_origin = request.POST.get('plant_origin_name')
            user_email = api.users(id=api.user).json()['email']
            # get data
            data = api.csi_import(
                params={
                    'origin': plant_origin,
                    'org_id': login_context['org_id'],
                    'user_email': user_email,
                    'format': 'json'}).json()
            if data['status'] == 'success':
                messages.success(request, "Your data has been requested and a task has been created to import the plants. We will email you when the plant import has finished.")
                return redirect('plant-import')
            else:
                messages.warning(request, data['status'])
                return redirect('plant-import')

    return render(request, 'core/plant_import.html', login_context)

@login_required
def plant_detail(request, pk, org_pk, *args, **kwargs):
    login_context = kwargs.pop('login_context')

    api = APIWrapper.from_session(login_context['sessionid'])

    plant_id = pk

    plant_detail = api.plants(plant_id).json()
    
    #display dc_rating as kW in app
    plant_detail.update({'dc_rating': plant_detail['dc_rating']/1000})

    login_context.update({
        'plant_detail': plant_detail,
        'weather_sources': api.weather_sources().json(),
        'environmental_conditions': api.environmental_conditions().json(),
        'pv_market_sectors': api.pv_market_sectors().json(),
        'cost_models': api.costmodels(failsafe=True).json(),
        'inverter_types': api.inverter_types().json(),
        'inverter_type_data': api.inverter_types().content,
        'module_types': api.module_types().json(),
        'inspection_techniques': api.inspection_techniques().json(),
        'mounting_locations': api.mounting_locations().json(),
        'mounting_types': api.mounting_types().json(),
        'roof_slope_types': api.roof_slope_types().json(),
        'roof_types': api.roof_types().json(),
        'tracking_types': api.tracking_types().json(),
        'states': states_dict[231],  # hardcoding US states
        'states_dict': json.dumps(states_dict),
        'cities': json.dumps(cities),
        'cities_dict': cities,  # required to select city on the record
        'country': countries,
    })

    # populate plant equipment and plant finance
    login_context.update({
        'plant_equipment': api.plant_equipments(
            id=login_context['plant_detail']['plant_equipment']
        ).json(),
        'plant_finance': api.plant_finances(
            id=login_context['plant_detail']['plant_finance']
        ).json(),
    })

    if 'GET' in request.method:
        return render(request, 'core/plant_detail.html', login_context)

    if 'POST' in request.method:
        if 'plant_update_btn' in request.POST:
            pf = PlantForm(request.POST)
            pf.is_valid()
            update_data = pf.cleaned_data
            # convert date back to text
            if not update_data.get('description'):
                update_data['description'] = ""

            update_data['activation_date'] = (
                pf.cleaned_data['activation_date'].strftime('%Y-%m-%d')
            )

            # FIX: cleaned_data loses environmental_condition selected values
            # populating here manually
            update_data['environmental_condition'] = (
                request.POST.getlist('environmental_condition')
            )
            update_data['cost_model'] = (
                request.POST.getlist('cost_model')
            )
            response = api.plants(method='patch', id=plant_id, data=update_data)
            login_context['plant_detail'] = response.json()
            messages.success(request, 'Success! Plant information updated.')
            return redirect('plant_detail', org_pk=login_context['org_id'], pk=plant_detail['id'])

        if 'plant_equipment_update_btn' in request.POST:
            form = PlantEquipmentForm(request.POST)
            form.is_valid()
            update_data = form.cleaned_data
            equipment_id = request.POST.get('equipment_id')
            response = api.plant_equipments(
                method='patch', id=equipment_id, data=update_data
            )
            login_context['plant_equipment'] = response.json()
            messages.success(request, 'Success! Plant Equipment Information Updated.')
            return redirect('plant_detail', org_pk=login_context['org_id'], pk=plant_detail['id'])

        if 'plant_finance_update_btn' in request.POST:
            form = PlantFinanceForm(request.POST)
            form.is_valid()
            update_data = form.cleaned_data
            finance_id = request.POST.get('finance_id')
            response = api.plant_finances(
                method='patch', id=finance_id, data=update_data
            )
            login_context['plant_finance'] = response.json()
            messages.success(request, 'Success! Plant Finance Information Updated.')
            return redirect('plant_detail', org_pk=login_context['org_id'], pk=plant_detail['id'])

        if 'plant_copy_btn' in request.POST:
            name = request.POST.get('name')

            plant = api.plants(id=plant_id).json()
            equipment = api.plant_equipments(id=plant['plant_equipment']).json()
            finance = api.plant_finances(id=plant['plant_finance']).json()

            # remove unique value attributes
            plant.pop('uuid')
            plant.pop('plant_report')
            plant.pop('upload_activity')
            finance.pop('id')
            equipment.pop('id')

            new_equipment = api.plant_equipments(
                method='post', data=equipment
            ).json()

            new_finance = api.plant_finances(
                method='post', data=finance
            ).json()

            plant['plant_finance'] = new_finance['id']
            plant['plant_equipment'] = new_equipment['id']
            plant['name'] = name

            new_plant = api.plants(method='post', data=plant).json()

            login_context['plant_detail'] = new_plant
            login_context['plant_equipment'] = new_equipment
            login_context['plant_finance'] = new_finance

            messages.success(
                request, 'Success! Plant {} copied.'.format(name)
            )
            return redirect('plant_detail', org_pk=login_context['org_id'], pk=new_plant['id'])

        if 'plant_del_btn' in request.POST:
            response = api.plants(method='delete', id=plant_id)
            return redirect('plant_list', org_pk=login_context['org_id'])

        if 'plant_export_btn' in request.POST:
            plant_id = pk
            plant_export_req = api.plant_export(params={'plant': str(plant_id)})
            if plant_export_req.status_code == 200:
                header = plant_export_req.headers['Content-Disposition']
                value, params = cgi.parse_header(header)
                filename = params['filename']

                response = HttpResponse(plant_export_req.content, content_type='application/ms-excel')
                response['Content-Disposition'] = 'attachment; filename='+filename
                return response

        return render(request, 'core/plant_detail.html', context)

@login_required
def plant_filter(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid'])

    states = api.states(params={'country': 231}).json()  # 231 -> US

    cities = {}
    for state in states:
        cities[state['id']] = api.cities(params={'state': state['id']}).json()

    login_context['form_url'] = reverse(
            'plant_list', kwargs={'org_pk': login_context['org_id']}
    )

    login_context.update({
        'states': states,
        'cities': json.dumps(cities),
        'button_name': 'plant_filter_btn',
    })

    return render(request, 'core/plant_filter.html', login_context)

@login_required
def release_notes(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')

    api = APIWrapper.from_session(login_context['sessionid'])

    return render(request, 'core/release_notes.html', login_context)

@login_required
def account_create(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')

    api = APIWrapper.from_session(login_context['sessionid'])

    user_permission_req = api.user_permissions(params={'format': 'json'})
    user_permission_list = user_permission_req.json()

    # get list of all organizations and pass to template
    if login_context['is_superuser'] == True:
        organizations_list = api.organizations().json()
    else:
        organizations_list = []

    login_context.update({
        'user_permission_list': user_permission_list,
        'organizations_list': organizations_list
    })

    if request.method == 'POST' and 'account_create_btn' in request.POST:
        f_name_ip = request.POST.get('f_name_ip')
        l_name_ip = request.POST.get('l_name_ip')
        email_ip = request.POST.get('email_ip')
        password_ip = request.POST.get('password_ip')
        permiss_ip = request.POST.get('permiss_ip')
        fma_ip = request.POST.getlist('fma_ip')
        if not fma_ip:
            fma_ip = 0
        else:
            fma_ip = 1

        # check if data posted is correct
        if login_context['is_superuser'] == True:
            organization_id = request.POST.get('organization_ip')
            user_data = {'first_name': f_name_ip, 'last_name': l_name_ip, 'email': email_ip, 'password': password_ip,
                     'user_permission': permiss_ip, 'organization': organization_id, 'failure_analysis_available': fma_ip}
            user_create_req = api.individual_user(method='post', data=user_data)
        else:
            user_data = {'first_name': f_name_ip, 'last_name': l_name_ip, 'email': email_ip, 'password': password_ip,
                         'user_permission': permiss_ip, 'organization': login_context['org_id'], 'failure_analysis_available': fma_ip}

            user_create_req = api.individual_user(method='post', data=user_data)
        if user_create_req.status_code == 201:
            messages.success(request, "Success! User Account created.")
        else:
            messages.warning(request, "Error! - " + str(user_create_req.content) + " - Please try again.")

    return render(request, 'core/account_create.html', login_context)

@login_required
def account_list(request, org_pk, *args, **kwargs):
    login_context = kwargs.pop('login_context')

    api = APIWrapper.from_session(login_context['sessionid'])

    if login_context['is_superuser'] == True:
        # returns json of all users
        account_list_data = api.users(params={'depth': 2}).json()
    else:
        # returns json of organizations users
        account_list_data = api.individual_user(params={'depth': 2}).json()

    login_context.update({'account_list': account_list_data})
    return render(request, 'core/account_list.html', login_context)

@login_required
def account_detail(request, pk, org_pk, *args, **kwargs):
    login_context = kwargs.pop('login_context')

    api = APIWrapper.from_session(login_context['sessionid'])

    if login_context['is_superuser'] == True:
        organizations_list = api.organizations().json()
    else:
        organizations_list = []

    if login_context['is_superuser'] == True:
        req = api.users(id=pk, params={'depth': 2})
        account_detail_data = req.json()
    else:
        req = api.individual_user(id=pk, params={'depth': 2})
        account_detail_data = req.json()

    if req.status_code == 404:
        raise Http404("Account does not exist")

    user_permission_req = api.user_permissions(params={'format': 'json'})
    user_permission_list = user_permission_req.json()

    login_context.update({
        'account_detail': account_detail_data,
        'user_permission_list': user_permission_list,
        'organizations_list': organizations_list,
    })
    if request.method == 'POST' and 'account_del_btn' in request.POST:
        account_del_req = api.users(method='delete', id=pk, params={'format': 'json'})
        if account_del_req.status_code == 204:
            messages.success(request, "Successfully Deleted User")
            return redirect('account_list', org_pk=login_context['org_id'])

    elif request.method == 'POST' and 'account_update_btn' in request.POST:
        pass_ip = request.POST.get('pass_ip')
        f_name_ip = request.POST.get('f_name_ip')
        l_name_ip = request.POST.get('l_name_ip')
        email_ip = request.POST.get('email_ip')
        permiss_ip = request.POST.get('permiss_ip')
        fma_ip = request.POST.getlist('fma_ip')
        organization_ip = request.POST.get('organization_ip')
        
        if not fma_ip:
            fma_ip = 0
        else:
            fma_ip = 1

        account_update_data = {'first_name': f_name_ip, 'last_name': l_name_ip, 'email': email_ip,
                           'user_permission': permiss_ip, 'password': pass_ip,
                           'failure_analysis_available': fma_ip, 'organization': organization_ip, 'is_active': True, 'is_superuser': False}
        
        if login_context['is_superuser']:
            account_update_data.update({'is_superuser': True})

        account_update_req = api.users(method='put', id=pk, params={'format': 'json'}, data=account_update_data)

        if account_update_req.status_code == 200:
            update_account = account_update_req.json()
            messages.success(request, "Success! User Account Updated. ")
            return redirect('account_list', org_pk=login_context['org_id'])
        else:
            messages.warning(request, "Error! - " + str(account_update_req.content) + " - Please try again.")

    return render(request, 'core/account_detail.html', login_context)


@login_required
def organization_list(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')

    api = APIWrapper.from_session(login_context['sessionid'])
    organization_list = api.organizations().json()

    context = {
        'organization_list': organization_list,
        'logged_in_user_id': login_context['user_id'],
    }

    context.update(login_context)

    return render(request, 'core/organization_list.html', context)


@login_required
def organization_detail(request, org_pk, *args, **kwargs):
    login_context = kwargs.pop('login_context')

    api = APIWrapper.from_session(login_context['sessionid'])

    if 'GET' in request.method:
        organization_detail = api.organizations(id=org_pk).json()
        login_context.update({
            'organization_detail': organization_detail,
        })

    if request.POST:
        if 'update' in request.POST:
            form = OrganizationForm(request.POST)
            form.is_valid()
            try:
                response = api.organizations(
                    method='put', id=org_pk, data=form.cleaned_data
                )
                login_context['organization_detail'] = response.json()
                messages.success(request, 'Success!')
            except APIResponseException as e:
                messages.warning(request, e.body)

        if 'delete' in request.POST:
            try:
                response = api.organizations(method='delete', id=org_pk)
                messages.success(request, 'Successfully deleted!')
                return redirect('organization_list')
            except APIResponseException as e:
                messages.warning(request, e.body)

    return render(request, 'core/organization_detail.html', login_context)


@login_required
def organization_create(request, org_pk, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    login_context.update({'new_organization': True})

    template = 'core/organization_detail.html'

    if 'GET' in request.method:
        return render(request, template, login_context)

    # POST
    form = OrganizationForm(request.POST)
    form.is_valid()

    api = APIWrapper.from_session(login_context['sessionid'])
    try:
        response = api.organizations(method='post', data=form.cleaned_data)
    except APIResponseException as e:
        messages.warning(request, e.body)
        login_context.update({'organization_detail': form.cleaned_data})
        return render(request, template, login_context)

    messages.success(request, 'Success! Organization created.')
    return redirect('organization_detail', org_pk=response.json()['id'])


