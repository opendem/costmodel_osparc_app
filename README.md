# Asset Performance Suite

The project is divided in two parts.

1. [Web Service](https://github.com/sunspec/costmodel_osparc_api).
2. [Web Application](https://github.com/sunspec/costmodel_osparc_app) - this project.

```note
You should have access to both repositories given above in order to setup environment for this AP suite.
```

For more information, refer [primary webservice project](https://github.com/sunspec/costmodel_osparc_api).
