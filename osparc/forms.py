import requests

from django import forms

from core.constants import BASE_URL

class PlantTimeSeriesForm(forms.Form):
    time_stamp = forms.DateTimeField()
    sample_interval = forms.IntegerField()
    WH_DIFF = forms.FloatField()
    GHI_DIFF = forms.FloatField()
    TMPAMB_AVG = forms.FloatField(required=False)
    HPOA_DIFF = forms.FloatField(required=False)
    record_status = forms.IntegerField()


class WeatherDataForm(forms.Form):
    site_name = forms.CharField()
    latitude = forms.FloatField()
    longitude = forms.FloatField()
    start_time = forms.DateTimeField()
    end_time = forms.DateTimeField()
