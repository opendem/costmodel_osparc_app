from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from osparc import views

urlpatterns = [
    url(r'^plant_time_series_create/$', views.plant_time_series_create, name='plant-time-series-create'),
    url(r'^plants/$', views.plants, name='plants'),
    url(r'^time-series-sample-download/$', views.time_series_sample_download, name='time-series-sample-download'),
    url(r'^plants/(?P<plant_pk>[0-9]+)/plant_record_data_detail$', views.plant_record_data_detail, name='time-series-list'),
    url(r'^weather-data-create/$', views.weather_data_create, name='weather-data-create'),
    url(r'^weather-data-plants/$', views.weather_data_plants, name='weather-data-plants'),
    url(r'^weather-data-plants/(?P<plant_pk>[0-9]+)/plants$', views.weather_data_list, name='weather-data-list'),
    url(r'^weather-data-sample-download/$', views.weather_data_sample_download, name='weather-data-sample-download'),
    url(r'^osparc/$', views.index, name='dashboard'),
    url(r'^osparc/create_report/$', views.create_report, name='create-report'),
    url(r'^osparc/create_report/(?P<custom_query>[0-9]+)/$', views.create_report, name='create-report'),
    url(r'^osparc/kpi_report_list/$', views.kpi_report_list, name='kpi-report-list'),
    url(r'^osparc/report_detail/(?P<pk>[0-9]+)/$', views.report_detail, name='report-detail'),
    url(r'^osparc/kpi_reports/$', views.report_detail_new, name='report-detail-new')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
