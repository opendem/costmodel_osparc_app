import requests
import csv
import json
import time
import MySQLdb

from bs4 import BeautifulSoup
from requests.auth import HTTPBasicAuth
import xml.etree.ElementTree as ET
import time
import os
from datetime import datetime
from dateparser import parse


from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.urls import reverse

from costmodel_osparc_app.settings import STATIC_ROOT
from core.constants import BASE_URL
from core.api import APIWrapper
from forms import (
    PlantTimeSeriesForm, WeatherDataForm
)

from core.constants import BASE_URL
from core.api import APIWrapper, APIResponseException
from core.views import login_required


@login_required
def index(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid'])

    stats = api.get_dashboard_stats().json()

    sc_array = stats['plants_count_by_state']
    sc_array.insert(0, ['State', 'Popularity'])

    total_plant_count = stats['total_plant_count_all']
    organization_plant_count = stats['total_plant_count_org']

    total_dc_capacity = stats['total_dc_sum_all']
    total_dc_for_org = stats['total_dc_sum_org']

    age_size_array = stats['year_dc_breakup']

    age_size_array.insert(0, [
        '', '<10kW', '<100kW', '<1MW', '<10MW', {'role': 'annotation'}
    ])

    kpis = api.kpis().json()

    if 'result' in kpis:
        kpis = json.loads(kpis['result'])
        kpis['dc_rating']['median'] = kpis['dc_rating']['median']/1000
        kpis['dc_rating']['maximum'] = kpis['dc_rating']['maximum']/1000
        kpis['dc_rating']['minimum'] = kpis['dc_rating']['minimum']/1000
        kpis['dc_rating']['mean'] = kpis['dc_rating']['mean']/1000

    login_context.update({
        'total_plant_count': total_plant_count,
        'organization_plant_count': organization_plant_count,
        'dc_capacity': int(total_dc_capacity.items()[0][-1]/1000.0),
        'total_dc_for_org': int(total_dc_for_org.items()[0][-1]/1000.0),
        'state_chart_array': json.dumps(sc_array),
        'age_size_array': json.dumps(age_size_array),
        'kpis': kpis,
    })

    return render(request, 'osparc/index.html', login_context)

@login_required
def create_report(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    login_context['custom_query'] = {}
    api = APIWrapper.from_session(login_context['sessionid'])

    states = api.states(params={'country': 231}).json()  # 231 -> US

    cities = {}
    for state in states:
        cities[state['id']] = api.cities(params={'state': state['id']}).json()

    cq_id = kwargs.get('custom_query', False)
    if cq_id:
        try:
            custom_queries_data = api.custom_queries(id=cq_id).json()
            custom_queries_data['api_query'] = json.loads(custom_queries_data['api_query'])
            login_context['custom_query'] = json.dumps(custom_queries_data)
        except APIResponseException:
            pass

    login_context.update({
        'states': states,
        'cities': json.dumps(cities),
        'form_url': reverse('report-detail-new'),
        'button_name': 'generate_report_btn',
    })

    return render(request, 'osparc/create_report.html', login_context)

@login_required
def kpi_report_list(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')

    api = APIWrapper.from_session(login_context['sessionid'])

    list_of_queries = api.custom_queries().json()
    for query in list_of_queries:
        query['date'] = parse(query['date'])

    username = api.individual_user(id=login_context['user_id']).json()

    login_context.update({'queries': list_of_queries, 'username': username})

    return render(request, 'osparc/kpi_report_list.html', login_context)

@login_required
def report_detail_new(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')

    api = APIWrapper.from_session(login_context['sessionid'])


    if request.method == 'POST' and 'generate_report_btn' in request.POST:
        # Since plant can be filtered by any of its attributes
        # and the attributes are added to filter criteria dynamically
        # we iterate over all params until we have consumed all.

        count = 1
        _continue = True
        filters = {}
        p = lambda x: '{}{}'.format(
            request.POST['attribute_%d' % x],
            request.POST['comparison_attribute_%d' % x]
        )

        while _continue:
            try:
                filters[p(count)] = request.POST['value_attribute_%d' % count]
                count += 1
            except KeyError:
                _continue = False
        filters.update(
            {
                'city': request.POST.get('city', ''),
                'state': request.POST.get('state', ''),
            }
        )
        plants_resp = api.kpis_multiple_plants(
            method="post",
            data={
                'plant_filters': json.dumps(filters),
                'start_time': request.POST['observation_start_date'],
                'end_time': request.POST['observation_end_date'],
                'org_id': login_context['org_id'],
                'report_name': request.POST['name'],
                'email_desired': request.POST.get('email_desired')
            }
        )
        status = plants_resp.json()['status']

        if 'successfully' not in status:
            messages.warning(request, status)
            return redirect('create-report')
        else:
            if request.POST.get('email_desired') != 'on':
                messages.success(request, "Report Successfully Scheduled")
            else:
                messages.success(request, status)

    return redirect('kpi-report-list')


@login_required
def report_detail(request, pk, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid']) 

    # get kpi based on id value of custom query
    query_pk = pk
    # the following code breaks:
    # custom_queries = api.custom_queries(id=query_pk).json()[0]
    # I removed the [0] so that the custom query doesn't fail
    custom_queries_data = api.custom_queries(id=query_pk).json()

    def return_markup(id='', label_text='', value=''):
        return """
            <div class=" col-md-3">
                <div class="form-group">
                    <label for="{id}"
                           class="col-md- control-label"
                    >{label_text}</label>
                    <input class="form-control"
                           id="{id}"
                           type="text"
                           value="{value}"
                           disabled
                    />
                </div>
            </div>
        """.format(id=id, label_text=label_text, value=value)

    filter_map = {
        'lt': 'less than', 'lte': 'less than or equal to',
        'gt': 'greater than', 'gte': 'greater than or equal to',
        'iexact': 'equal to', 'contains': 'contains'
    }

    api_query = custom_queries_data['api_query']
    query_filters = []
    try:
        filters = json.loads(api_query)
        for k, v in filters.items():
            if 'state' in k:
                v = api.states(id=v).json()['name']
            if 'city' in k:
                v = api.cities(id=v).json()['name']
            if '__' in k:
                key, key_filter = k.split('__')
                key_filter = filter_map[key_filter]
                k = '{} ({})'.format(key, key_filter)
            query_filters.append(
                return_markup(id=k.lower().split('(')[0], label_text=k.title(), value=v)
            )
    except:
        pass

    login_context['query_filters'] = query_filters

    if custom_queries_data['kpi_result']:
        custom_queries_data.update({
            'kpi_result': json.loads(custom_queries_data['kpi_result'])
        })

    login_context.update({
        'kpi': custom_queries_data['kpi_result'],
        'report_name': custom_queries_data['report_name'],
        'cq_data': custom_queries_data,
    })

    if 'rerun_report_btn' in request.POST:
        plants_resp = api.kpis_multiple_plants(
            method="post",
            data={
                'plant_filters': custom_queries_data['api_query'],
                'start_time': custom_queries_data['start_date'],
                'end_time': custom_queries_data['end_date'],
                'org_id': login_context['org_id'],
                'report_name': custom_queries_data['report_name'],
                'email_desired': 'false',
                'cq_id': query_pk,
            }
        )
        status = plants_resp.json()['status']

        if 'successfully' in status:
            messages.success(request, "Report successfully scheduled for re-run.")

    if 'delete_report_btn' in request.POST:
        custom_queries = api.custom_queries(method='delete', id=query_pk)
        messages.success(request, "The Report was Successfully Deleted")
        return redirect('kpi-report-list')

    if 'copy_report_btn' in request.POST:
        messages.warning(
            request,
            "This form is using query parameters from an existing report "
            "to generate a new report."
        )
        return redirect('create-report', custom_query=query_pk)

    # for now, we will offer export as csv of data
    if 'query_export_btn' in request.POST:
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="kpi_report.csv"'

        writer = csv.writer(response)
        writer.writerow(['KPI', 'Mean', 'Median', 'Minimum', 'Maximum'])

        # Update names for the kpi report csv file
        data = custom_queries_data['kpi_result']

        data['Monthly Insolation (kWh/m2)'] = data.pop('monthly_insolation')
        data['Storage Capacity (kWh)'] = data.pop('storage_capacity')
        data['Monthly AC Energy (MWh)'] = data.pop('monthly_yield')
        data['DC Power Rating (kW)'] = data.pop('dc_rating')
        data['Storage State of Health'] = data.pop('storage_state_of_health')
        data['Monthly DC Energy (kWh)'] = data.pop('monthly_generated_energy')
        if data.get('performance_ratio'):
            data['Performance Ratio'] = data.pop('performance_ratio')

        for k, v in data.iteritems():
            writer.writerow([k, v['mean'], v['median'], v['minimum'], v['maximum']])

        return response

    login_context['username'] = api.individual_user(id=login_context['user_id']).json()
    return render(request, 'osparc/report_detail.html', login_context)

@login_required
def plant_time_series_create(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid']) 

    if 'GET' in request.method:
        response = api.plants()
        plant_list_data = response.json()['results']
        login_context.update({'plant_list': plant_list_data})

        return render(request, 'osparc/plant_time_series_create.html', login_context)
        
        
    if request.method == 'POST' and 'plant_import_btn' in request.POST:
        datafile = request.FILES['datafile']
        Reader = csv.reader(datafile)
        Data = list(Reader)

        file_name = STATIC_ROOT + '/core/example_files/TimeSeriesSample.csv'
        file = open(file_name, 'rb')
        read_template = csv.reader(file)
        all_data = list(read_template)

        # check if template matches uploaded file
        if all_data[0] != Data[0]:
            messages.warning(request, "The file that you uploaded does not match the template. Please download the template provided and follow the same format.")
            return redirect('plant-time-series-create')

        all_ts_data = []
        plant_id = request.GET.get('plant_id')
        for data in Data[1:]:
            if data[1]:
                try:
                    value = int(data[1])
                    Data.pop(1)
                    Data.insert(1, value)
                except:
                    messages.warning(request, "The values for Sample Interval must be a number with no decimals, please try again after you update your spreadsheet.")
                    return redirect('plant-time-series-create')
            if data[2]:
                try:
                    value = float(data[2])
                    Data.pop(2)
                    Data.insert(2, value)
                except:
                    messages.warning(request, "The values for WH_DIFF must be a number, please try again after you update your spreadsheet.")
                    return redirect('plant-time-series-create')
            if data[3]:
                try:
                    value = float(data[3])
                    Data.pop(3)
                    Data.insert(3, value)
                except:
                    messages.warning(request, "The values for GHI_DIFF must be a number, please try again after you update your spreadsheet.")
                    return redirect('plant-time-series-create')
            if data[4]:
                try:
                    value = float(data[4])
                    Data.pop(4)
                    Data.insert(4, value)
                except:
                    messages.warning(request, "The values for TMPAMB_AVG must be a number, please try again after you update your spreadsheet.")
                    return redirect('plant-time-series-create')
            if data[5]:
                try:
                    value = float(data[5])
                    Data.pop(5)
                    Data.insert(5, value)
                except:
                    messages.warning(request, "The values for HPOA_DIFF must be a number, please try again after you update your spreadsheet.")
                    return redirect('plant-time-series-create')

            ts_data = {
                'time_stamp': data[0],
                'sample_interval': data[1], # required, int
                'WH_DIFF': data[2], # required, float
                'GHI_DIFF': data[3], #float
                'TMPAMB_AVG': data[4], # float
                'HPOA_DIFF': data[5], # float
                'record_status': data[6],
                'plant': plant_id
            }
            all_ts_data.append(ts_data)

        for data in all_ts_data:
            plant_time_series = api.plant_time_series(method='post', id=plant_id, data=data).json()

        messages.success(request, "Time Series Successfully created!")
        return redirect('time-series-list', plant_pk=plant_id)

    return render(request, 'osparc/plant_time_series_create.html', login_context)

@login_required
def plants(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid']) 

    response = api.plants()
    list_of_plants = response.json()['results']

    login_context.update({'plants_list': list_of_plants})
    
    return render(request, 'osparc/plants.html', login_context)

@login_required
def plant_record_data_detail(request, plant_pk, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid'])

    plant_id = plant_pk

    plant = api.plants(plant_id).json()
    time_series_data = api.plant_time_series(plant_id).json()
    
    for query in time_series_data:
        query['time_stamp'] = parse(query['time_stamp'])

    login_context.update({
        'plant_time_series': time_series_data,
        'plant_data': plant,
    })

    return render(request, 'osparc/plant_record_data_detail.html', login_context)

def time_series_sample_download(request):
    filename = 'TimeSeriesSample.csv'
    file_location = STATIC_ROOT + '/core/example_files/TimeSeriesSample.csv'
    csv_file = open(file_location, 'r+')

    response = HttpResponse(csv_file, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename

    return response

def get_weather_data(site_name, latitude, longitude, start_time, end_time):
    url = "https://data.solaranywhere.com/api/v2/WeatherData"
    username = "APSuite@sunspec.org"
    password = "}%N[ja)8u#"
    querystring = {"key":"SUNSX3F2"}
    payload = """<CreateWeatherDataRequest xmlns="http://service.solaranywhere.com/api/v2">
             <Sites>
              <Site Name="{}" Latitude="{}" Longitude="{}" />
             </Sites>
             <Options
             WeatherDataSource="SolarAnywhere3_2"
             WeatherDataPreference = "Auto"
             PerformTimeShifting = "false"
             StartTime="{}"
             EndTime="{}"
             SpatialResolution_Degrees="0.1"
             TimeResolution_Minutes="60"
             OutputFields="StartTime,ObservationTime,EndTime,GlobalHorizontalIrradiance_WattsPerMeterSquared,DirectNormalIrradiance_WattsPerMeterSquared,DiffuseHorizontalIrradiance_WattsPerMeterSquared,IrradianceObservationType,AmbientTemperature_DegreesC,AmbientTemperatureObservationType,WindSpeed_MetersPerSecond,WindSpeedObservationType,WindDirection_Degrees,RelativeHumidity_Percent,SnowDepth_Meters,LiquidPrecipitation_KilogramsPerMeterSquared,SolidPrecipitation_KilogramsPerMeterSquared"
             SummaryOutputFields="TotalGlobalHorizontalIrradiance"/>
            </CreateWeatherDataRequest>""".format(site_name, latitude, longitude, start_time, end_time)
    headers = {
        'content-type': "text/xml; charset=utf-8",
        'content-length': "length",
    }
    response = requests.post(url,auth = HTTPBasicAuth(username,password),data=payload,headers=headers,params=querystring)
    root = ET.fromstring(response.content)
    public_id = root.attrib.get("WeatherRequestId")

    if public_id == None:
        return {'status': 'Data Not available at this time.'}

    #GET WeatherDataResult
    url2 = "https://data.solaranywhere.com/api/v2/WeatherDataResult/"
    request_number = 0
    max_request_number = 100
    while(request_number < max_request_number):
        data = requests.get(url2 + public_id,auth = HTTPBasicAuth(username,password))
        radicle = ET.fromstring(data.content)
        status = radicle.attrib.get("Status")
        if status == 'Done':
            return data.content
        else:
            request_number = request_number + 1

@login_required
def weather_data_create(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid'])

    plant_list = api.plants().json()['results']

    login_context.update({'plant_list': plant_list})

    if request.method == 'POST' and 'weather_data_btn' in request.POST:
        plant_id = request.GET.get('plant_id')
        plant_data = api.plants(plant_id).json()

        # for site name, we need to use city per WeatherData API
        site_name = plant_data['name']
        latitude = plant_data['latitude']
        longitude = plant_data['longitude']
        # need conditional if no lat, long was ever entered for plant, maybe redirect with message
        if not (site_name or latitude or longitude):
            messages.warning(request, "Oops, it looks like you are missing your city, latitude, or longitude. Please make sure you have filled out a city, latitude, and longitude for your plant and then weather data will work!")
            return redirect('plant_detail', org_pk=org_id, pk=plant_id)

        start_time = parse(request.POST.get('start_time'))
        end_time = parse(request.POST.get('end_time'))

        if start_time > parse('today') or end_time > parse('today'):
            messages.warning(request, "It looks like the date that you entered was too far in the future, please select a different date and time and try again.")
            return redirect('weather-data-create')

        time_tpl = '%Y-%m-%dT00:00:00-%H:%M'

        stfmt = start_time.strftime(time_tpl)
        etfmt = end_time.strftime(time_tpl)

        payload = {
            'site_name': site_name,
            'latitude': latitude,
            'longitude': longitude,
            'stfmt': stfmt,
            'etfmt': etfmt,
        }

        create_weather_data = api.solar_anywhere_create_weather_data(id=plant_id, params=payload)

        if create_weather_data.status_code == 200:
            messages.success(request, "Successfully Recieved Request for Weather Data")
            return redirect('weather-data-create')
        else:
            messages.warning(request, "Solar Anywhere failed, please try again later")
            return redirect('weather-data-create')

    elif request.method == 'POST' and 'import_weather_data_btn' in request.POST:
        datafile = request.FILES['datafile']
        Reader = csv.reader(datafile)
        Data = list(Reader)

        plant_id = request.GET.get('plant_id')

        for data in Data[1:]:
            weather_data = {
                'start_time': "",
                'observation_time': data[0],
                'end_time': "",
                'global_horizontal_irradiance': data[1],
                'direct_normal_irradiance': data[2],
                'diffuse_horizontal_irradiance': data[3],
                'irradiance_observation_type': data[4],
                'ambient_temperature': data[5],
                'ambient_temperature_observation_type': data[6],
                'wind_speed': data[7],
                'wind_speed_observation_type': data[8],
                'relative_humidity': data[9],
                'liquid_precipitation': data[10],
                'solid_precipitation': data[11],
                'snow_depth': data[12],
                'plant': plant_id,
            }

            response = api.weather_data(method='post', id=plant_id, data=weather_data)
            upload_csv = response.json()

        if response.status_code in [200, 201]:
            messages.success(request, "Successfully Created Weather Data!")
        else:
            message.failure(request, "Failed uploading file.")

        return redirect('weather-data-list', plant_pk=plant_id)

    return render(request, 'osparc/weather_data_create.html', login_context)

@login_required
def weather_data_plants(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid'])

    response = api.plants()
    list_of_plants = response.json()['results']

    login_context.update({'plants_list': list_of_plants})

    return render(request, 'osparc/weather_data_plants.html', login_context)

@login_required
def weather_data_list(request, plant_pk, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid'])
    
    plant_id = plant_pk

    plant = api.plants(plant_id).json()
    weather_data = api.weather_data(plant_id).json()

    login_context.update({
        'weather_data': weather_data,
        'plant_data': plant,
    })

    return render(request, 'osparc/weather_data_list.html', login_context)

def weather_data_sample_download(request):
    filename = 'WeatherDataExample.csv'
    file_location = STATIC_ROOT + '/core/example_files/WeatherDataExample.csv'
    csv_file = open(file_location, 'r+')
    response = HttpResponse(csv_file, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename

    return response
