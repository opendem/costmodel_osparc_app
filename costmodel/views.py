from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.models import AnonymousUser
from django.shortcuts import render, redirect
from django.contrib import messages
from core.constants import BASE_URL
import requests
import json
import sys
import ast
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
import cgi
from openpyxl import load_workbook

from core.api import APIWrapper
from core.views import login_required


@login_required
@csrf_exempt
def costmodels_dropdown(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid'])

    costmodels = api.costmodels(params={'id__in': request.POST.get('costmodel_ids', '')}).json()

    return JsonResponse(costmodels, safe=False)


@login_required
@csrf_exempt
def reports(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid'])

    plants_req = api.plants_list_no_pagination()
    plant_list_data = plants_req.json()

    plant_group_req = api.plant_groups(params={'format': 'json'})
    plant_group_list_data = plant_group_req.json()

    login_context.update({
        'plant_list_data': plants_req.content,
        'plant_list': plant_list_data['data'],
        'plant_group_list': plant_group_list_data,
    })

    if 'POST' in request.method:
        if 'create_plant_report_btn' in request.POST:
            cm_id = request.POST.get('cm_ip', '')
            plant_id = request.POST.get('plant_ip', '')
            results_req = api.results(params={
                'format': 'json',
                'costmodel': cm_id,
                'plant': plant_id,
                'organization': login_context['org_id']
            })
            messages.success(request, "Plant Report Successfully Created")
            return redirect('reports-list', org_pk=login_context['org_id'])

        if 'create_plant_group_report_btn' in request.POST:
            plant_group_id = request.POST.get('plant_group_ip', '')
            group_results_req = api.plant_group_results(params={
                'format': 'json',
                'plant_group': plant_group_id,
                'organization': login_context['org_id'],
            })
            
            if 'error' in group_results_req.json()['status']:
                messages.warning(request, "Plant Group Report Failed, please try again")
                return redirect('reports-list', org_pk=login_context['org_id'])
            else:
                messages.success(request, "Plant Group Report Successfully Created")
                return redirect('reports-list', org_pk=login_context['org_id'])


    return render(request, 'costmodel/reports.html', login_context)

@login_required
@csrf_exempt
def reports_list(request, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid'])
    
    plant_reports = api.get_plant_report().json()

    login_context.update({
        'reports_list': True,
        'plant_reports': plant_reports,
    })

    return render(request, 'osparc/kpi_report_list.html', login_context)

@login_required
@csrf_exempt
def reports_detail(request, pk, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid'])

    report_pk = pk
    report_req = api.get_plant_report(id=report_pk)
    report_name = report_req.json()['report_name']
    login_context.update({'report_name': report_name, 'report_pk': report_pk})

    if 'POST' in request.method:
        if 'delete_rpt_btn' in request.POST:
            response = api.get_plant_report(method='delete', id=report_pk)
            if response.status_code == 204:
                messages.success(request, "Report Successfully Deleted")
                return redirect('reports-list', org_pk=login_context['org_id'])
            else:
                messages.warning(request, "Report Not Deleted, please try again or contact the system administrator.")
                return redirect('reports-list', org_pk=login_context['org_id'])
        if 'rerun_report_btn' in request.POST:
            cm_id = report_req.json()['costmodel']
            plant_id = report_req.json()['plant']

            results_req = api.results(params={
                'format': 'json',
                'costmodel': cm_id,
                'plant': plant_id,
                'organization': login_context['org_id']
            })

            if 'success' in results_req.json()['status']:
                messages.success(request, "Plant Report Successfully Re Ran")
                return redirect('reports-list', org_pk=login_context['org_id'])
            else:
                messages.warning(request, "Plant Report could not be re run, please try again.")
                return redirect('reports-list', org_pk=login_context['org_id'])

    if report_req.status_code == 200:
        
        results = report_req.json()['result']
        results = json.loads(results)
        report_req_content = json.loads(report_req.content)
        report_req_content = str(report_req_content['result'])
        
        colors = ["#E45D24", "#0B62A4", "#F0AD4E"]

        npv_by_svc_data = map(None, results['npv_by_activity'], colors)

        mapped_results = map(None, results['total_labor_cost'], results['total_material_and_other_cost'],
                             results['total_cost_for_measure'], results['average_annual_expense'],
                             results['npv_project_life'], results['percentage_of_total_npv'],
                             results['annual_cash_flow'], results['service_list'], results['service_instance_list'],
                             results['reserve_account'], results['total_labor_hours_per_year'])

        mapped_failure_results = map(None, results['dict_failure_mode_analysis'], results['dict_failure_mode_analysis'])

        login_context.update({
            'json_data': report_req_content,
            'results': results,
            'mapped_results': mapped_results,
            'mapped_failure_results': mapped_failure_results,
            'npv_by_svc_data': npv_by_svc_data,
        })

        return render(request, 'costmodel/reports_detail.html', login_context)

    else:
        messages.warning(request, "Error! - Bad Request - Please try again.")
        return redirect('reports-list', org_pk=login_context['org_id'])


@login_required
@csrf_exempt
def group_reports_detail(request, pk, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid'])
    report_pk = pk
    report_req = api.get_plant_report(id=report_pk)

    report_name = report_req.json()['report_name']
    login_context.update({'report_name': report_name, 'report_pk': report_pk})

    if 'POST' in request.method:
        if 'delete_rpt_btn' in request.POST:
            response = api.get_plant_report(method='delete', id=report_pk)
            if response.status_code == 204:
                messages.success(request, "Report Successfully Deleted")
                return redirect('reports-list', org_pk=login_context['org_id'])
            else:
                messages.warning(request, "Report Not Deleted, please try again or contact the system administrator.")
                return redirect('reports-list', org_pk=login_context['org_id'])
        if 'rerun_report_btn' in request.POST:
            plant_group_id = report_req.json()['plant_group']

            group_results_req = api.plant_group_results(params={
                'format': 'json',
                'plant_group': plant_group_id,
                'organization': login_context['org_id'],
            })
            
            if 'success' in group_results_req.json()['status']:
                messages.success(request, "Plant Report Successfully Re Ran")
                return redirect('reports-list', org_pk=login_context['org_id'])
            else:
                messages.warning(request, "Plant Report could not be re run, please try again.")
                return redirect('reports-list', org_pk=login_context['org_id'])

    if report_req.status_code == 200:
        results = report_req.json()['result']
        results = json.loads(results)
        report_req_content = json.loads(report_req.content)
        report_req_content = str(report_req_content['result'])

        npv_by_svc_data = map(None, results['npv_by_activity'])

        login_context.update({
            'json_data': report_req_content,
            'results': results,
            'npv_by_svc_data': npv_by_svc_data,
        })        
        return render(request, 'costmodel/plant_group_reports_detail.html', login_context)
    else:
        messages.warning(request, "Error! - Bad Request - Please try again.")
        return redirect('reports-list', org_pk=login_context['org_id'])

@login_required
def costmodel_list(request, org_pk, *args, **kwargs):
    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid'])

    default = request.GET.get('default', '')

    req = api.costmodels(params={'depth': 1, 'format': 'json', 'default': default})
    costmodel_list_data = req.json()

    if login_context['org_id'] != 1:
        default_costmodel_data = [cm_data for cm_data in costmodel_list_data if cm_data['defined_by']['id'] == 1]
        org_costmodels = [cm_data for cm_data in costmodel_list_data[4:] if cm_data['organization']['id'] == login_context['org_id']]
        costmodel_list_data = default_costmodel_data + org_costmodels

    login_context.update({
        'costmodel_list': costmodel_list_data,
        'default': default,
    })

    return render(request, 'costmodel/costmodel_list.html', login_context)

@login_required
def costmodel_detail(request, pk, org_pk, *args, **kwargs):
    try:
        org_id = request.session['org_id']
        sessionid = request.session['sessionid']
        csrftoken = request.session['csrftoken']
        user_permission = request.session['user_permission']
        cost_model_license = request.session['cost_model_license']
        osparc_license = request.session['osparc_license']
    except KeyError:
        return redirect('auth')

    cookies = {'sessionid': sessionid, 'csrftoken': csrftoken}
    headers = {'content-type': 'application/json', 'X-CSRFToken': csrftoken}

    login_context = kwargs.pop('login_context')
    api = APIWrapper.from_session(login_context['sessionid'])
    costmodel_org_id = org_pk
    costmodel_id = pk
    req = requests.get(BASE_URL + '/organizations/'+str(costmodel_org_id)+'/cost_models/'+str(costmodel_id)+'?depth=3&format=json',
                       cookies=cookies)
    #req = api.costmodels(id=costmodel_id, params={'depth': 3, 'format': 'json'})
    costmodel_detail_data = req.json()

    plants_in_cm_req = requests.get(BASE_URL + '/organizations/' + str(costmodel_org_id) + '/cost_models/' + str(
        costmodel_id) + '?format=json', cookies=cookies)
    #plants_in_cm_req = api.costmodels(id=costmodel_id, params={'format': 'json'})
    plants_in_cm = plants_in_cm_req.json()
    my_plant_list = plants_in_cm['plant']

    plants_req = requests.get(BASE_URL + '/organizations/' + str(org_id) + '/plants/?format=json', cookies=cookies)
    #plants_req = api.plants()
    plants = plants_req.json()

    if req.status_code == 404:
        raise Http404("Cost Model does not exist")

#    login_context.update({
#        'my_plant_list': my_plant_list,
#        'costmodel_detail': costmodel_detail_data,
#        'plants': plants,
#    })
    services_req = requests.get(BASE_URL + '/organizations/'+str(org_id)+'/services/?depth=1&default=true&format=json',
                                cookies=cookies)
    #service_req = api.services(params={'depth': 1, 'default': 'true', 'format': 'json'})
    services_data = services_req.json()

    allservices = []
    for service in services_data:
        allservices.append(service['id'])

#    login_context.update({
#        'om_types': api.om_types().json(),
#        'asset_types': api.asset_types().json(),
#        'service_types': api.service_types().json(),
#        'warranty_types': api.warranty_types().json(),
#        'service_providers': api.service_providers().json(),
#        'failure_distributions': api.failure_distribution_types().json(),
#    })

    om_types_req = requests.get(BASE_URL + '/om_types/?format=json', cookies=cookies)
    om_types = om_types_req.json()

    service_types_req = requests.get(BASE_URL + '/service_types/?format=json', cookies=cookies)
    service_types = service_types_req.json()

    asset_type_req = requests.get(BASE_URL + '/asset_types/?format=json', cookies=cookies)
    asset_types = asset_type_req.json() 

    applicable_unit_req = requests.get(BASE_URL + '/applicable_units/?format=json', cookies=cookies)
    applicable_unit = applicable_unit_req.json()

    warranty_types_req = requests.get(BASE_URL + '/warranty_types/?format=json', cookies=cookies)
    warranty_types = warranty_types_req.json()

    service_providers_req = requests.get(BASE_URL + '/service_providers/?format=json', cookies=cookies)
    service_providers = service_providers_req.json()

    failure_distributions_req = requests.get(BASE_URL + '/failure_distribution_types/?format=json', cookies=cookies)
    failure_distributions = failure_distributions_req.json()

    services_in_cm_req = requests.get(BASE_URL + '/organizations/'+str(costmodel_org_id)+'/cost_models/'+str(costmodel_id)+'?format=json',
                                      cookies=cookies)
    services_in_cm_data = services_in_cm_req.json()

    # Finding the services that do not belong to the cost model - Missing Services
    difference = list(set(allservices) - set(services_in_cm_data['service_values']))

    missing_services = []
    for id in difference:
        for service in services_data:
            if id == service['id']:
                missing_services.append(service)

    if request.method == 'POST' and 'add_service_btn' in request.POST:
        new_services_ip = request.POST.getlist('new_services_ip')

        x = services_in_cm_data['service_values']
        y = [unicode(i) for i in x]
        new_services = y + new_services_ip
        data = {'name': costmodel_detail_data['name'], 'organization': org_id, 'service_values': new_services,
                'defined_by': 2, 'plant': my_plant_list}

        add_services_req = requests.put(BASE_URL + '/organizations/'+str(costmodel_org_id)+'/cost_models/'+str(costmodel_id)+'/?format=json',
                                        headers=headers, data=json.dumps(data), cookies=cookies)
        if add_services_req.status_code == 200:
            costmodel_org_id = org_pk
            costmodel_id = pk
            req = requests.get(BASE_URL + '/organizations/'+str(costmodel_org_id)+'/cost_models/'+str(costmodel_id)+'?depth=3&format=json',
                               cookies=cookies)
            costmodel_detail_data = req.json()
            # messages.success(request, "Success! Services Added.")
            return render(request, 'costmodel/costmodel_detail.html',
                          {'costmodel_detail': costmodel_detail_data,
                           'services': costmodel_detail_data['service_values'], 'missing_services': missing_services,
                           'om_types': om_types, 'service_types': service_types, 'asset_types': asset_types,
                           'applicable_unit': applicable_unit, 'warranty_types': warranty_types,
                           'service_providers': service_providers, 'failure_distributions': failure_distributions,
                           'org_id': org_id, 'user_permission': user_permission, 'plants': plants,
                           'my_plant_list': my_plant_list, 'cost_model_license': cost_model_license,
                           'osparc_license': osparc_license})

        #else:
            # messages.warning(request, "Error! Please try again.")

    elif request.method == 'POST' and 'remove_service_btn' in request.POST:
        service_id_ip = request.POST.get('service_id_ip')
        print('Service id = '+str(service_id_ip))
        to_remove = []
        to_remove.append(service_id_ip)
        x = services_in_cm_data['service_values']
        y = [unicode(i) for i in x]
        new_services = list(set(y) - set(to_remove))
        data = {'name': costmodel_detail_data['name'], 'organization': org_id, 'service_values': new_services,
                'defined_by': 2, 'plant': my_plant_list}

        remove_services_req = requests.put(BASE_URL + '/organizations/'+str(costmodel_org_id)+'/cost_models/'+str(costmodel_id)+'/?format=json',
                                           headers=headers, data=json.dumps(data), cookies=cookies)
        print(remove_services_req.content)
        if remove_services_req.status_code == 200:
            costmodel_org_id = org_pk
            costmodel_id = pk
            req = requests.get(BASE_URL + '/organizations/'+str(costmodel_org_id)+'/cost_models/'+str(costmodel_id)+'?depth=3&format=json',
                               cookies=cookies)
            costmodel_detail_data = req.json()

            plants_in_cm_req = requests.get(BASE_URL + '/organizations/' + str(costmodel_org_id) + '/cost_models/' + str(
                costmodel_id) + '?format=json', cookies=cookies)
            plants_in_cm = plants_in_cm_req.json()
            my_plant_list = plants_in_cm['plant']

            # messages.success(request, "Success! Service Removed.")
            return render(request, 'costmodel/costmodel_detail.html',
                          {
                              'costmodel_detail': costmodel_detail_data,
                              'services': costmodel_detail_data['service_values'], 'missing_services': missing_services,
                              'om_types': om_types, 'service_types': service_types, 'asset_types': asset_types,
                              'applicable_unit': applicable_unit, 'warranty_types': warranty_types,
                              'service_providers': service_providers, 'failure_distributions': failure_distributions,
                              'org_id': org_id, 'user_permission': user_permission, 'plants': plants,
                              'my_plant_list': my_plant_list, 'cost_model_license': cost_model_license,
                              'osparc_license': osparc_license
                          })
        # else:
            # messages.warning(request, "Error! Please try again.")

    elif request.method == 'POST' and 'cm_update_btn' in request.POST:
        cm_name_ip = request.POST.get('cm_name_ip')
        cm_desc_ip = request.POST.get('cm_desc_ip')
        plants_ip = request.POST.getlist('plants_ip')
        costmodel_org_id = org_pk
        costmodel_id = pk
        req = requests.get(BASE_URL + '/organizations/'+str(costmodel_org_id)+'/cost_models/'+str(costmodel_id)+'?format=json',
                           cookies=cookies)
        services_data = req.json()
        data = {'name': cm_name_ip,
                'description': cm_desc_ip,
                'organization': org_id,
                'service_values': services_data['service_values'],
                'defined_by': 2,
                'plant': plants_ip
                }
        cm_update_req = requests.put(BASE_URL+'/organizations/'+str(costmodel_org_id)+'/cost_models/'+str(costmodel_id)+'/?format=json',
                                     headers=headers, data=json.dumps(data), cookies=cookies)

        if cm_update_req.status_code == 200:
            costmodel_org_id = org_pk
            costmodel_id = pk
            req = requests.get(BASE_URL + '/organizations/'+str(costmodel_org_id)+'/cost_models/'+str(costmodel_id)+'?depth=3&format=json',
                               cookies=cookies)
            costmodel_detail_data = req.json()

            plants_in_cm_req = requests.get(BASE_URL + '/organizations/' + str(costmodel_org_id) + '/cost_models/' + str(
                costmodel_id) + '?format=json', cookies=cookies)
            plants_in_cm = plants_in_cm_req.json()
            my_plant_list = plants_in_cm['plant']

            messages.success(request, "Success! Cost Model Updated.")
            return render(request, 'costmodel/costmodel_detail.html',
                          {
                              'costmodel_detail': costmodel_detail_data,
                              'services': costmodel_detail_data['service_values'], 'missing_services': missing_services,
                              'om_types': om_types, 'service_types': service_types, 'asset_types': asset_types,
                              'applicable_unit': applicable_unit, 'warranty_types': warranty_types,
                              'service_providers': service_providers, 'failure_distributions': failure_distributions,
                              'org_id': org_id, 'user_permission': user_permission, 'plants': plants,
                              'my_plant_list': my_plant_list, 'cost_model_license': cost_model_license,
                              'osparc_license': osparc_license
                          })
        else:
            messages.warning(request, "Error! - "+str(cm_update_req.content)+" - Please try again.")

    elif request.method == 'POST' and 'cm_copy_btn' in request.POST:
        cm_name_ip = request.POST.get('cm_name_ip')
        cm_desc_ip = request.POST.get('cm_desc_ip')

        cm_req = requests.get(
            BASE_URL + '/organizations/'+str(org_pk)+'/cost_models/'+str(pk)+'/?format=json', cookies=cookies)
        cm = cm_req.json()

        cm_data = {'name': cm_name_ip, 'description': cm_desc_ip,
                   'service_values': cm['service_values'], 'organization': org_id,
                   'defined_by': 2, 'plant': cm['plant']}

        cm_create_req = requests.post(BASE_URL+'/organizations/'+str(org_id)+'/cost_models/?format=json',
                                      headers=headers, data=json.dumps(cm_data), cookies=cookies)

        if cm_create_req.status_code == 201:
            new_cm = cm_create_req.json()
            new_cm_id = new_cm['id']
            messages.success(request, "Success! Cost Model Copied. ")
            return redirect('/organizations/' + str(org_id) + '/costmodels/' + str(new_cm_id))
        else:
            messages.warning(request, "Error! - " + str(cm_create_req.content) + " - Please try again.")

    elif request.method == 'POST' and 'cm_del_btn' in request.POST:
        costmodel_org_id = org_pk
        costmodel_id = pk
        cm_del_req = requests.delete(BASE_URL + '/organizations/'+str(costmodel_org_id)+'/cost_models/'+str(costmodel_id)+'?format=json',
                                     headers=headers, cookies=cookies)
        if cm_del_req.status_code == 204:
            return redirect('/organizations/'+str(org_id)+'/costmodels/?default=true')

    return render(request, 'costmodel/costmodel_detail.html',
                  {
                      'costmodel_detail': costmodel_detail_data, 'services': costmodel_detail_data['service_values'],
                      'missing_services': missing_services, 'om_types': om_types, 'service_types': service_types,
                      'asset_types': asset_types, 'applicable_unit': applicable_unit, 'warranty_types': warranty_types,
                      'service_providers': service_providers, 'failure_distributions': failure_distributions,
                      'org_id': org_id, 'user_permission': user_permission, 'plants': plants,
                      'my_plant_list': my_plant_list, 'cost_model_license': cost_model_license,
                      'osparc_license': osparc_license, 'is_superuser': login_context['is_superuser'],
                  })


def costmodel_create(request):

    try:
        org_id = request.session['org_id']
        sessionid = request.session['sessionid']
        csrftoken = request.session['csrftoken']
        user_permission = request.session['user_permission']
        cost_model_license = request.session['cost_model_license']
        osparc_license = request.session['osparc_license']
    except KeyError:
        return redirect('auth')

    cookies = {'sessionid': sessionid, 'csrftoken': csrftoken}
    headers = {'content-type': 'application/json', 'X-CSRFToken': csrftoken}

    api = APIWrapper.from_session(sessionid)
    is_superuser = api.is_superuser

    base_costmodel = api.default_costmodel_values().json()

    plants_req = requests.get(BASE_URL+'/organizations/'+str(org_id)+'/no-pagination-plants/?format=json', cookies=cookies)
    plants = plants_req.json()['data']

    if request.method == 'POST' and 'cm_create_btn' in request.POST:
        cm_name_ip = request.POST.get('cm_name_ip')
        cm_desc_ip = request.POST.get('cm_desc_ip')
        services_ip = request.POST.get('services_ip')
        service_values = ast.literal_eval(services_ip)
        plants_ip = request.POST.getlist('plants_ip')

        data = {'name': cm_name_ip,
                'description': cm_desc_ip,
                'organization': org_id,
                'service_values': service_values,
                'defined_by': 2,
                'plant': plants_ip
                }
        cm_create_req = requests.post(BASE_URL+'/organizations/'+str(org_id)+'/cost_models/?format=json',
                                      headers=headers, data=json.dumps(data), cookies=cookies)

        if cm_create_req.status_code == 201:
            messages.success(request, "Success! Cost Model created.")
        else:
            messages.warning(request, "Error! - "+str(cm_create_req.content)+" - Please try again.")

    return render(request, 'costmodel/costmodel_create.html',
                  {
                      'base_costmodel': base_costmodel, 'org_id': org_id, 'user_permission': user_permission,
                      'plants': plants, 'cost_model_license': cost_model_license, 'osparc_license': osparc_license,
                      'is_superuser': is_superuser
                  })


def service_create(request):

    try:
        org_id = request.session['org_id']
        sessionid = request.session['sessionid']
        csrftoken = request.session['csrftoken']
        user_permission = request.session['user_permission']
        cost_model_license = request.session['cost_model_license']
        osparc_license = request.session['osparc_license']
    except KeyError:
        return redirect('auth')

    cookies = {'sessionid': sessionid, 'csrftoken': csrftoken}
    headers = {'content-type': 'application/json', 'X-CSRFToken': csrftoken}

    api = APIWrapper.from_session(sessionid)
    is_superuser = api.is_superuser

    om_types_req = requests.get(BASE_URL + '/om_types/?format=json', cookies=cookies)
    om_types = om_types_req.json()

    service_type_req = requests.get(BASE_URL + '/service_types/?format=json', cookies=cookies)
    service_types = service_type_req.json()

    asset_type_req = requests.get(BASE_URL + '/asset_types/?format=json', cookies=cookies)
    asset_types = asset_type_req.json()

    applicable_unit_req = requests.get(BASE_URL + '/applicable_units/?format=json', cookies=cookies)
    applicable_unit = applicable_unit_req.json()

    warranty_types_req = requests.get(BASE_URL + '/warranty_types/?format=json', cookies=cookies)
    warranty_types = warranty_types_req.json()

    service_providers_req = requests.get(BASE_URL + '/service_providers/?format=json', cookies=cookies)
    service_providers = service_providers_req.json()

    labor_rates_req = requests.get(BASE_URL + '/organizations/'+str(org_id)+'/labor_rates/?default=true&format=json',
                                   cookies=cookies)
    labor_rates_data = labor_rates_req.json()

    cm_req = requests.get(BASE_URL+'/organizations/'+str(org_id)+'/cost_models/?default=false&format=json',
                          cookies=cookies)
    costmodels = cm_req.json()

    failure_distributions_req = requests.get(BASE_URL + '/failure_distribution_types/?format=json', cookies=cookies)
    failure_distributions = failure_distributions_req.json()

    if request.method == 'POST' and 'service_create_btn' in request.POST:

        om_type_ip = request.POST.get('om_type_ip')
        service_type_ip = request.POST.get('service_type_ip')
        asset_type_ip = request.POST.get('asset_type_ip')
        service_name_ip = request.POST.get('service_name_ip')
        service_desc_ip = request.POST.get('service_desc_ip')
        service_values_name_ip = request.POST.get('service_values_name_ip')
        service_values_desc_ip = request.POST.get('service_values_desc_ip')
        app_unit_ip = request.POST.get('app_unit_ip')
        matl_warranty_ip = request.POST.get('matl_warranty_ip')
        lab_warranty_ip = request.POST.get('lab_warranty_ip')
        warranty_type_ip = request.POST.get('warranty_type_ip')
        service_prov_ip = request.POST.get('service_prov_ip')
        lab_rate_ip = request.POST.get('lab_rate_ip')
        interval_ip = request.POST.get('interval_ip')
        shape_factor_ip = request.POST.get('shape_factor_ip')
        distribution_ip = request.POST.get('distribution_ip')
        # units_ip = request.POST.get('units_ip')
        # lab_hours_ip = request.POST.get('lab_hours_ip')
        # matl_cost_ip = request.POST.get('matl_cost_ip')
        notes_ip = request.POST.get('notes_ip')
        reference_ip = request.POST.get('reference_ip')
        cm_ip = request.POST.getlist('cm_ip')

        service_data = {'name': service_name_ip, 'description': service_desc_ip, 'om_type': om_type_ip,
                        'service_type': service_type_ip, 'asset_type': asset_type_ip,
                        'organization': org_id, 'defined_by': 2
                        }

        service_create_req = requests.post(BASE_URL+'/organizations/'+str(org_id)+'/services/?format=json',
                                           headers=headers, data=json.dumps(service_data), cookies=cookies)
        service_info = service_create_req.json()

        if service_create_req.status_code == 201:
            service_id = service_info['id']
            service_name = service_info['name']
            organization = service_info['organization']
            service_values_data = {
                'name': service_values_name_ip, 'description': service_values_desc_ip,
                'labor_warranty_covered': lab_warranty_ip,
                'material_warranty_covered': matl_warranty_ip, 'mean_interval': interval_ip,
                'shape_factor': shape_factor_ip,
                # 'no_of_units': units_ip, 'labor_hours_per_unit': lab_hours_ip,
                # 'material_cost_per_unit': matl_cost_ip,
                'notes': notes_ip, 'reference': reference_ip, 'service': service_id, 'warranty_type': warranty_type_ip,
                'applicable_unit': app_unit_ip, 'service_provider': service_prov_ip, 'labor_rate': lab_rate_ip,
                'failure_distribution_type': distribution_ip, 'organization': org_id, 'defined_by': 2,
                'cost_model': cm_ip}
            service_values_create_req = requests.post(BASE_URL + '/organizations/'+str(org_id)+'/service_values/?format=json',
                                                      headers=headers, data=json.dumps(service_values_data), cookies=cookies)
            service_values_info = service_values_create_req.json()

            if service_values_create_req.status_code == 201:
                messages.success(request, "Success! Service created.")

                #service_values_id = service_values_info['id']
                #service_id = service_info['id']
                #service_update_data = {'name': service_name, 'organization': organization, 'default_values': service_values_id}
                #service_update_req = requests.put(BASE_URL + '/organizations/'+str(org_id)+'/services/'+str(service_id)+'/?format=json',
                #                                  cookies=cookies, headers=headers, data=json.dumps(service_update_data))
                #if service_update_req.status_code == 200:
                #    messages.success(request, "Success! Service created.")
                #else:
                #    messages.warning(request, "Error! - "+str(service_update_req.content)+" - Please try again.")
            else:
                messages.warning(request, "Error! - "+str(service_values_create_req.content)+" - Please try again.")
        else:
            messages.warning(request, "Error! - "+str(service_create_req.content)+" - Please try again.")

    # filter default costmodels
    if not is_superuser:
        costmodels = [c for c in costmodels if c['defined_by'] != 1]

    return render(request, 'costmodel/service_create.html',
                  {
                      'om_types': om_types, 'service_types': service_types, 'asset_types': asset_types,
                      'applicable_unit': applicable_unit, 'warranty_types': warranty_types,
                      'service_providers': service_providers, 'labor_rates_raw': labor_rates_req.content,
                      'labor_rates': labor_rates_data, 'failure_distributions': failure_distributions, 'org_id': org_id,
                      'user_permission': user_permission, 'costmodels': costmodels,
                      'cost_model_license': cost_model_license, 'osparc_license': osparc_license,
                      'is_superuser': is_superuser
                  })


def service_list(request, org_pk):
    try:
        org_id = request.session['org_id']
        sessionid = request.session['sessionid']
        user_permission = request.session['user_permission']
        cost_model_license = request.session['cost_model_license']
        osparc_license = request.session['osparc_license']
    except KeyError:
        return redirect('auth')

    default = request.GET.get('default', '')

    cookies = {'sessionid': sessionid}
    api = APIWrapper.from_session(sessionid)
    is_superuser = api.is_superuser

    req = requests.get(
      BASE_URL +
      '/organizations/'+
      str(org_id) +
      '/service_values/?fields!=cost_model,failure_mode_values&default='+str(default)+'&format=json&depth=2',
                       cookies=cookies)
    list_data = req.json()
    
    if org_id != 1:
        response = api.default_service_values(params={'depth': '2', 'fields!': 'cost_model,failure_mode_values'})
        default_service = response.json()
        list_data = list_data + default_service

    return render(request, 'costmodel/service_list.html',
                  {
                      'service_list': list_data, 'org_id': org_id, 'default': default,
                      'user_permission': user_permission, 'cost_model_license': cost_model_license,
                      'osparc_license': osparc_license, 'is_superuser': is_superuser
                  })


def service_detail(request, pk, org_pk):
    try:
        org_id = request.session['org_id']
        user_permission = request.session['user_permission']
        cost_model_license = request.session['cost_model_license']
        osparc_license = request.session['osparc_license']
        sessionid = request.session['sessionid']
        csrftoken = request.session['csrftoken']
    except KeyError:
        return redirect('auth')

    cookies = {'sessionid': sessionid, 'csrftoken': csrftoken}
    headers = {'content-type': 'application/json', 'X-CSRFToken': csrftoken}

    api = APIWrapper.from_session(sessionid)
    is_superuser = api.is_superuser

    new_service_value_id = None

    req = requests.get(BASE_URL + '/organizations/'+str(org_pk)+'/service_values/'+str(pk)+'?depth=2&format=json',
                       cookies=cookies)
    service_values_detail_data = req.json()

    if req.status_code == 404:
        raise Http404("Service does not exist")

    om_types_req = requests.get(BASE_URL + '/om_types/?format=json', cookies=cookies)
    om_types = om_types_req.json()

    service_type_req = requests.get(BASE_URL + '/service_types/?format=json', cookies=cookies)
    service_types = service_type_req.json()

    asset_type_req = requests.get(BASE_URL + '/asset_types/?format=json', cookies=cookies)
    asset_types = asset_type_req.json()

    applicable_unit_req = requests.get(BASE_URL + '/applicable_units/?format=json', cookies=cookies)
    applicable_unit = applicable_unit_req.json()

    warranty_types_req = requests.get(BASE_URL + '/warranty_types/?format=json', cookies=cookies)
    warranty_types = warranty_types_req.json()

    service_providers_req = requests.get(BASE_URL + '/service_providers/?format=json', cookies=cookies)
    service_providers = service_providers_req.json()

    labor_rates_req = requests.get(BASE_URL + '/organizations/'+str(org_id)+'/labor_rates/?default=true&format=json',
                                   cookies=cookies)
    labor_rates_data = labor_rates_req.json()

    failure_distributions_req = requests.get(BASE_URL + '/failure_distribution_types/?format=json', cookies=cookies)
    failure_distributions = failure_distributions_req.json()

    costmodels = api.default_costmodel_values().json()
    
    cm_in_service_req = requests.get(BASE_URL+'/organizations/'+str(org_pk)+'/service_values/'+str(pk)+'?format=json',
                                     cookies=cookies)
    cm_in_service = cm_in_service_req.json()
    my_cm_list = cm_in_service['cost_model']

    if request.method == 'POST' and 'svc_save_btn' in request.POST:
        om_type_ip = request.POST.get('om_type_ip')
        service_type_ip = request.POST.get('service_type_ip')
        asset_type_ip = request.POST.get('asset_type_ip')
        service_name_ip = request.POST.get('service_name_ip')
        service_desc_ip = request.POST.get('service_desc_ip')
        service_values_name_ip = request.POST.get('service_values_name_ip')
        service_values_desc_ip = request.POST.get('service_values_desc_ip')
        app_unit_ip = request.POST.get('app_unit_ip')
        matl_warranty_ip = request.POST.get('matl_warranty_ip')
        lab_warranty_ip = request.POST.get('lab_warranty_ip')
        warranty_type_ip = request.POST.get('warranty_type_ip')
        service_prov_ip = request.POST.get('service_prov_ip')
        lab_rate_ip = request.POST.get('lab_rate_ip')
        interval_ip = request.POST.get('interval_ip')
        shape_factor_ip = request.POST.get('shape_factor_ip')
        distribution_ip = request.POST.get('distribution_ip')
        # units_ip = request.POST.get('units_ip')
        # lab_hours_ip = request.POST.get('lab_hours_ip')
        # matl_cost_ip = request.POST.get('matl_cost_ip')
        notes_ip = request.POST.get('notes_ip')
        reference_ip = request.POST.get('reference_ip')
        service_id_ip = request.POST.get('service_id_ip')
        cm_ip = request.POST.getlist('cm_ip')

        if app_unit_ip == '':
            app_unit_ip = None

        service_values_data = {
            'name': service_values_name_ip, 'description': service_values_desc_ip,
            'labor_warranty_covered': lab_warranty_ip, 'material_warranty_covered': matl_warranty_ip,
            'mean_interval': interval_ip, 'shape_factor': shape_factor_ip,
            # 'no_of_units': units_ip, 'labor_hours_per_unit': lab_hours_ip,
            # 'material_cost_per_unit': matl_cost_ip,
            'notes': notes_ip, 'reference': reference_ip, 'service': service_id_ip,
            'warranty_type': warranty_type_ip, 'applicable_unit': app_unit_ip,
            'service_provider': service_prov_ip, 'labor_rate': lab_rate_ip,
            'failure_distribution_type': distribution_ip, 'organization': org_id,
            'defined_by': 2, 'cost_model': cm_ip
        }

        service_values_update_req = requests.put(BASE_URL + '/organizations/'+str(org_pk)+'/service_values/'+str(pk)+'/?format=json',
                                          headers=headers, data=json.dumps(service_values_data), cookies=cookies)
        service_values_info = service_values_update_req.json()

        if service_values_update_req.status_code == 200:
            service_id = service_values_info['service']
            service_req = requests.get(BASE_URL + '/organizations/'+str(org_id)+'/services/'+str(service_id)+'/?format=json',
                                            cookies=cookies)
            if service_req.status_code == 200:
                service_info = service_req.json()
                service_organization = service_info['organization']
            elif service_req.status_code == 404:
                service_req = requests.get(BASE_URL + '/organizations/2/services/' + str(service_id) + '/?format=json',
                                                cookies=cookies)
                service_info = service_req.json()
                service_organization = service_info['organization']
            if service_organization != 2:
                service_data = {'name': service_name_ip, 'description': service_desc_ip,
                                     'om_type': om_type_ip, 'service_type': service_type_ip,
                                     'asset_types': asset_type_ip, 'organization': org_id, 'defined_by': 2
                                     }
                service_type_update_req = requests.put(BASE_URL+'/organizations/'+str(service_organization)+'/services/'+str(service_id)+'/?format=json',
                                                       headers=headers, data=json.dumps(service_data), cookies=cookies)
                if service_type_update_req.status_code == 200:
                    messages.success(request, "Success! Service Updated.")
                    req = requests.get(BASE_URL + '/organizations/'+str(org_pk)+'/service_values/'+str(pk)+'?depth=2&format=json',
                                       cookies=cookies)
                    service_values_detail_data = req.json()
                    cm_in_service_req = requests.get(
                        BASE_URL + '/organizations/' + str(org_pk) + '/service_values/' + str(pk) + '?format=json',
                        cookies=cookies)
                    cm_in_service = cm_in_service_req.json()
                    my_cm_list = cm_in_service['cost_model']
                    return render(request, 'costmodel/service_detail.html',
                                  {
                                      'service_detail': service_values_detail_data, 'om_types': om_types,
                                      'service_types': service_types, 'asset_types': asset_types,
                                      'applicable_unit': applicable_unit, 'warranty_types': warranty_types,
                                      'service_providers': service_providers,
                                      'labor_rates_raw': labor_rates_req.content, 'labor_rates': labor_rates_data,
                                      'failure_distributions': failure_distributions,
                                      'new_service_value_id': new_service_value_id, 'org_id': org_id,
                                      'user_permission': user_permission, 'costmodels': costmodels,
                                      'my_cm_list': my_cm_list, 'cost_model_license': cost_model_license,
                                      'osparc_license': osparc_license
                                  })
                else:
                    messages.warning(request, "Error! - " + str(service_type_update_req.content) + " - Please try again.")
            else:
                messages.success(request, "Success! Service Updated.")
                req = requests.get(BASE_URL + '/organizations/'+str(org_pk)+'/service_values/' + str(pk)+'?depth=2&format=json',
                                   cookies=cookies)
                service_values_detail_data = req.json()
                cm_in_service_req = requests.get(BASE_URL + '/organizations/'+str(org_pk)+'/service_values/'+str(pk)+'?format=json', cookies=cookies)
                cm_in_service = cm_in_service_req.json()
                my_cm_list = cm_in_service['cost_model']
                return render(request, 'costmodel/service_detail.html',
                              {
                                  'service_detail': service_values_detail_data, 'om_types': om_types,
                                  'service_types': service_types, 'asset_types': asset_types,
                                  'applicable_unit': applicable_unit, 'warranty_types': warranty_types,
                                  'service_providers': service_providers, 'labor_rates_raw': labor_rates_req.content,
                                  'labor_rates': labor_rates_data, 'failure_distributions': failure_distributions,
                                  'new_service_value_id': new_service_value_id, 'org_id': org_id,
                                  'user_permission': user_permission, 'costmodels': costmodels,
                                  'my_cm_list': my_cm_list, 'cost_model_license': cost_model_license,
                                  'osparc_license': osparc_license
                              })
        else:
            messages.warning(request, "Error! - " + str(service_values_update_req.content) + " - Please try again.")

    elif request.method == 'POST' and 'svc_save_as_btn' in request.POST:
        service_values_name_ip = request.POST.get('service_values_name_ip')
        service_values_desc_ip = request.POST.get('service_values_desc_ip')

        service_values_req = requests.get(BASE_URL + '/organizations/'+str(org_pk)+'/service_values/'+str(pk)+'/?depth=2&format=json',
                                   cookies=cookies)
        service_values = service_values_req.json()
        service = service_values['service']

        if service_values['applicable_unit'] is not None:
            app_unit = service_values['applicable_unit']['id']
        else:
            app_unit = None

        service_values_data = {'name': service_values_name_ip, 'description': service_values_desc_ip,
                               'labor_warranty_covered': service_values['labor_warranty_covered'],
                               'material_warranty_covered': service_values['material_warranty_covered'],
                               'mean_interval': service_values['mean_interval'],
                               'shape_factor': service_values['shape_factor'],
                               'number_of_units': service_values['number_of_units'],
                               'labor_hours_per_unit': service_values['labor_hours_per_unit'],
                               'material_cost_per_unit': service_values['material_cost_per_unit'],
                               'notes': service_values['notes'], 'reference': service_values['reference'],
                               'warranty_type': service_values['warranty_type']['id'],
                               'applicable_unit': app_unit,
                               'service_provider': service_values['service_provider']['id'],
                               'labor_rate': service_values['labor_rate']['id'],
                               'failure_distribution_type': service_values['failure_distribution_type']['id'],
                               'organization': org_id, 'service': service['id'], 'defined_by': 2,
                               'cost_model': []}

        service_values_create_req = requests.post(BASE_URL+'/organizations/'+str(org_id)+'/service_values/?format=json',
                                           headers=headers, data=json.dumps(service_values_data), cookies=cookies)

        if service_values_create_req.status_code == 201:
            new_service_value = service_values_create_req.json()
            new_service_value_id = new_service_value['id']
            messages.success(request, "Success! Service Copied. ")
            return redirect('/organizations/'+str(org_id)+'/services/'+str(new_service_value_id))
        else:
            messages.warning(request, "Error! - " + str(service_values_create_req.content) + " - Please try again.")

    elif request.method == 'POST' and 'svc_del_btn' in request.POST:
            svc_del_req = requests.delete(BASE_URL + '/organizations/'+str(org_pk)+'/service_values/'+str(pk)+'?format=json',
                                          headers=headers, cookies=cookies)
            if svc_del_req.status_code == 204:
                return redirect('/organizations/'+str(org_id)+'/services/?default=true')

    return render(request, 'costmodel/service_detail.html',
                  {
                      'service_detail': service_values_detail_data, 'om_types': om_types,
                      'service_types': service_types, 'asset_types': asset_types, 'applicable_unit': applicable_unit,
                      'warranty_types': warranty_types, 'service_providers': service_providers,
                      'labor_rates_raw': labor_rates_req.content, 'labor_rates': labor_rates_data,
                      'failure_distributions': failure_distributions, 'new_service_value_id': new_service_value_id,
                      'org_id': org_id, 'user_permission': user_permission, 'costmodels': costmodels,
                      'my_cm_list': my_cm_list, 'cost_model_license': cost_model_license,
                      'osparc_license': osparc_license, 'is_superuser': is_superuser
                  })


def labor_rates(request, org_pk):
    try:
        org_id = request.session['org_id']
        sessionid = request.session['sessionid']
        user_permission = request.session['user_permission']
        cost_model_license = request.session['cost_model_license']
        osparc_license = request.session['osparc_license']
    except KeyError:
        return redirect('auth')

    default = request.GET.get('default', '')

    cookies = {'sessionid': sessionid}

    api = APIWrapper.from_session(sessionid)
    is_superuser = api.is_superuser

    #default_req = requests.get(BASE_URL + '/organizations/'+str(org_id)+'/labor_rates/?default='+str(default)+'&depth=1&format=json',
    #                           cookies=cookies)
    #json_list = default_req.json()
    default_req = api.labor_rates(params={'default': default, 'depth': 1, 'format': 'json'})
    json_list = default_req.json()
    
    return render(request, 'costmodel/labor_rates.html',
                  {
                      'labor_rate_data': json_list, 'org_id': org_id, 'default': default,
                      'user_permission': user_permission, 'cost_model_license': cost_model_license,
                      'osparc_license': osparc_license, 'is_superuser': is_superuser
                  })


def labor_rates_create(request):
    try:
        org_id = request.session['org_id']
        sessionid = request.session['sessionid']
        csrftoken = request.session['csrftoken']
        user_permission = request.session['user_permission']
        cost_model_license = request.session['cost_model_license']
        osparc_license = request.session['osparc_license']
    except KeyError:
        return redirect('auth')

    cookies = {'sessionid': sessionid, 'csrftoken': csrftoken}
    headers = {'content-type': 'application/json', 'X-CSRFToken': csrftoken}

    api = APIWrapper.from_session(sessionid)
    is_superuser = api.is_superuser

    country_req = requests.get(BASE_URL + '/countries/?format=json', cookies=cookies)
    country_list = country_req.json()

    state_req = requests.get(BASE_URL + '/states/?format=json', cookies=cookies)
    city_req = requests.get(BASE_URL + '/cities/?format=json', cookies=cookies)

    sp_req = requests.get(BASE_URL + '/service_providers/?format=json', cookies=cookies)
    sp_list = sp_req.json()

    if request.method == 'POST' and 'lr_create_btn' in request.POST:
        name_ip = request.POST.get('name_ip')
        desc_ip = request.POST.get('desc_ip')
        rate_ip = request.POST.get('rate_ip')
        mul_ip = request.POST.get('mul_ip')
        sp_ip = request.POST.get('sp_ip')
        country_ip = request.POST.get('country_ip')
        state_ip = request.POST.get('state_ip')
        city_ip = request.POST.get('city_ip')

        if country_ip == '':
            country_ip = None

        if state_ip == '':
            state_ip = None

        if city_ip == '':
            city_ip = None

        lr_data = {'name': name_ip, 'description': desc_ip, 'rate_per_hour': rate_ip, 'overhead_multiplier': mul_ip,
                   'service_provider': sp_ip, 'country': country_ip, 'state': state_ip, 'city': city_ip,
                   'organization': org_id, 'defined_by': 2}
        lr_create_req = requests.post(BASE_URL + '/organizations/'+str(org_id)+'/labor_rates/?format=json',
                                      headers=headers, data=json.dumps(lr_data), cookies=cookies)
        if lr_create_req.status_code == 201:
            messages.success(request, "Success! Labor Rate added. ")
        else:
            messages.warning(request, "Error! - " + str(lr_create_req.content) + " - Please try again.")

    elif request.method == 'POST' and 'add_country_btn' in request.POST:
        country_name_ip = request.POST.get('country_name_ip')
        country_code_ip = request.POST.get('country_code_ip')

        country_data = {'name': country_name_ip, 'code': country_code_ip}
        country_create_req = requests.post(BASE_URL + '/countries/?format=json', headers=headers,
                                           data=json.dumps(country_data), cookies=cookies)
        if country_create_req.status_code == 201:
            messages.success(request, "Success! Country added. ")
            country_req = requests.get(BASE_URL + '/countries/?format=json')
            country_list = country_req.json()
            return render(request, 'costmodel/labor_rate_create.html',
                          {
                              'country': country_list, 'state': state_req.content, 'city': city_req.content,
                              'service_provider': sp_list, 'org_id': org_id, 'user_permission': user_permission,
                              'cost_model_license': cost_model_license, 'osparc_license': osparc_license
                          })
        else:
            messages.warning(request, "Error! - " + str(country_create_req.content) + " - Please try again.")

    elif request.method == 'POST' and 'add_state_btn' in request.POST:
        country_ip = request.POST.get('country_ip')
        state_name_ip = request.POST.get('state_name_ip')

        state_data = {'name': state_name_ip, 'country': country_ip}
        state_create_req = requests.post(BASE_URL + '/states/?format=json', headers=headers,
                                         data=json.dumps(state_data), cookies=cookies)
        if state_create_req.status_code == 201:
            messages.success(request, "Success! State added. ")
            state_req = requests.get(BASE_URL + '/states/?format=json')
            return render(request, 'costmodel/labor_rate_create.html',
                          {
                              'country': country_list, 'state': state_req.content, 'city': city_req.content,
                              'service_provider': sp_list, 'org_id': org_id, 'user_permission': user_permission,
                              'cost_model_license': cost_model_license, 'osparc_license': osparc_license
                          })
        else:
            messages.warning(request, "Error! - " + str(state_create_req.content) + " - Please try again.")

    elif request.method == 'POST' and 'add_city_btn' in request.POST:
        state_ip = request.POST.get('state_ip')
        city_name_ip = request.POST.get('city_name_ip')

        city_data = {'name': city_name_ip, 'state': state_ip}
        city_create_req = requests.post(BASE_URL + '/cities/?format=json', headers=headers,
                                        data=json.dumps(city_data), cookies=cookies)
        if city_create_req.status_code == 201:
            messages.success(request, "Success! City added. ")
            city_req = requests.get(BASE_URL + '/cities/?format=json')
            return render(request, 'costmodel/labor_rate_create.html',
                          {'country': country_list, 'state': state_req.content, 'city': city_req.content,
                           'service_provider': sp_list, 'org_id': org_id, 'user_permission': user_permission,
                           'cost_model_license': cost_model_license, 'osparc_license': osparc_license
                           })
        else:
            messages.warning(request,
                             "Error! - " + str(city_create_req.content) + " - Please try again.")

    return render(request, 'costmodel/labor_rate_create.html',
                  {
                      'country': country_list, 'state': state_req.content, 'city': city_req.content,
                      'service_provider': sp_list, 'org_id': org_id, 'user_permission': user_permission,
                      'cost_model_license': cost_model_license, 'osparc_license': osparc_license,
                      'is_superuser': is_superuser
                  })


def labor_rate_detail(request, pk, org_pk):
    try:
        org_id = request.session['org_id']
        user_permission = request.session['user_permission']
        cost_model_license = request.session['cost_model_license']
        osparc_license = request.session['osparc_license']
        sessionid = request.session['sessionid']
        csrftoken = request.session['csrftoken']
    except KeyError:
        return redirect('auth')

    cookies = {'sessionid': sessionid, 'csrftoken': csrftoken}
    headers = {'content-type': 'application/json', 'X-CSRFToken': csrftoken}

    api = APIWrapper.from_session(sessionid)
    is_superuser = api.is_superuser

    req = requests.get(BASE_URL + '/organizations/'+str(org_pk)+'/labor_rates/'+str(pk)+'?depth=1&format=json',
                       cookies=cookies)
    lr_detail_data = req.json()

    if req.status_code == 404:
        raise Http404("Labor Rate does not exist")

    country_req = requests.get(BASE_URL + '/countries/?format=json', cookies=cookies)
    country_list = country_req.json()

    state_req = requests.get(BASE_URL + '/states/?format=json', cookies=cookies)
    city_req = requests.get(BASE_URL + '/cities/?format=json', cookies=cookies)

    sp_req = requests.get(BASE_URL + '/service_providers/?format=json', cookies=cookies)
    sp_list = sp_req.json()

    if request.method == 'POST' and 'lr_del_btn' in request.POST:
        lr_del_req = requests.delete(BASE_URL + '/organizations/'+str(org_pk)+'/labor_rates/'+str(pk)+'?format=json',
                                     headers=headers, cookies=cookies)
        if lr_del_req.status_code == 204:
            return redirect('/organizations/'+str(org_id)+'/labor_rates/?default=true')

    elif request.method == 'POST' and 'lr_update_btn' in request.POST:
        name_ip = request.POST.get('name_ip')
        desc_ip = request.POST.get('desc_ip')
        rate_ip = request.POST.get('rate_ip')
        mul_ip = request.POST.get('mul_ip')
        sp_ip = request.POST.get('sp_ip')
        country_ip = request.POST.get('country_ip')
        state_ip = request.POST.get('state_ip')
        city_ip = request.POST.get('city_ip')

        if country_ip == '':
            country_ip = None

        if state_ip == '':
            state_ip = None

        if city_ip == '':
            city_ip = None

        lr_update_data = {'name': name_ip, 'description': desc_ip, 'rate_per_hour': rate_ip,
                          'overhead_multiplier': mul_ip, 'service_provider': sp_ip,
                          'country': country_ip, 'state': state_ip, 'city': city_ip,
                          'organization': org_id, 'defined_by': 2}
        print(lr_update_data)
        lr_update_req = requests.put(BASE_URL + '/organizations/'+str(org_pk)+'/labor_rates/'+str(pk)+'/?format=json',
                                     headers=headers, data=json.dumps(lr_update_data), cookies=cookies)
        print(lr_update_req.content)
        if lr_update_req.status_code == 200:
            messages.success(request, "Success! Labor Rate Updated. ")
            labor_rate_id = pk
            req = requests.get(BASE_URL + '/organizations/'+str(org_pk)+'/labor_rates/'+str(labor_rate_id)+'?depth=1&format=json',
                               cookies=cookies)
            lr_detail_data = req.json()
            return render(request, 'costmodel/labor_rate_detail.html',
                          {
                              'lr_detail': lr_detail_data, 'lr_detail_js': req.content, 'country': country_list,
                              'state': state_req.content, 'city': city_req.content, 'service_provider': sp_list,
                              'org_id': org_id, 'user_permission': user_permission,
                              'cost_model_license': cost_model_license, 'osparc_license': osparc_license
                          })

        else:
            messages.warning(request, "Error! - " + str(lr_update_req.content) + " - Please try again.")

    return render(request, 'costmodel/labor_rate_detail.html',
                  {
                      'lr_detail': lr_detail_data, 'lr_detail_js': req.content, 'country': country_list,
                      'state': state_req.content, 'city': city_req.content, 'service_provider': sp_list,
                      'org_id': org_id, 'user_permission': user_permission,
                      'cost_model_license': cost_model_license, 'osparc_license': osparc_license,
                      'is_superuser': is_superuser
                  })


def plant_group_create(request):
    try:
        org_id = request.session['org_id']
        sessionid = request.session['sessionid']
        csrftoken = request.session['csrftoken']
        user_permission = request.session['user_permission']
        cost_model_license = request.session['cost_model_license']
        osparc_license = request.session['osparc_license']
    except KeyError:
        return redirect('auth')

    cookies = {'sessionid': sessionid, 'csrftoken': csrftoken}
    headers = {'content-type': 'application/json', 'X-CSRFToken': csrftoken}

    api = APIWrapper.from_session(sessionid)
    is_superuser = api.is_superuser

    plants_req = requests.get(BASE_URL+'/organizations/'+str(org_id)+'/no-pagination-plants/?format=json', cookies=cookies)
    plants = plants_req.json()['data']

    plant_groups_req = requests.get(BASE_URL + '/organizations/' + str(org_id) + '/plant_groups/?format=json', cookies=cookies)
    plant_groups = plant_groups_req.json()

    if request.method == 'POST' and 'plant_group_create_btn' in request.POST:
        name_ip = request.POST.get('name_ip')
        desc_ip = request.POST.get('desc_ip')
        plants_ip = request.POST.getlist('plants_ip')
        plant_groups_ip = request.POST.getlist('plant_groups_ip')

        plant_group_data = {'name': name_ip, 'description': desc_ip, 'plant': plants_ip,
                            'plant_group': plant_groups_ip, 'organization': org_id}
        print(plant_group_data)
        plant_group_create_req = requests.post(BASE_URL + '/organizations/'+str(org_id)+'/plant_groups/?format=json',
                                               headers=headers, data=json.dumps(plant_group_data), cookies=cookies)
        print(plant_group_create_req.content)
        if plant_group_create_req.status_code == 201:
            messages.success(request, "Success! Plant Group created.")
        else:
            messages.warning(request, "Error! - " + str(plant_group_create_req.content) + " - Please try again.")

    return render(request, 'costmodel/plant_group_create.html',
                  {
                      'org_id': org_id, 'user_permission': user_permission, 'plants': plants,
                      'plant_groups': plant_groups, 'cost_model_license': cost_model_license,
                      'osparc_license': osparc_license, 'is_superuser': is_superuser
                  })


def plant_group_list(request, org_pk):
    try:
        org_id = request.session['org_id']
        sessionid = request.session['sessionid']
        user_permission = request.session['user_permission']
        cost_model_license = request.session['cost_model_license']
        osparc_license = request.session['osparc_license']
    except KeyError:
        return redirect('auth')

    cookies = {'sessionid': sessionid}

    api = APIWrapper.from_session(sessionid)
    is_superuser = api.is_superuser

    req = requests.get(BASE_URL + '/organizations/' + str(org_id) + '/plant_groups/?format=json&depth=1',
                       cookies=cookies)
    plant_group_list_data = req.json()

    return render(request, 'costmodel/plant_group_list.html',
                  {
                      'org_id': org_id, 'user_permission': user_permission, 'plant_group_list': plant_group_list_data,
                      'cost_model_license': cost_model_license, 'osparc_license': osparc_license,
                      'is_superuser': is_superuser
                  })


def plant_group_detail(request, pk, org_pk):
    try:
        org_id = request.session['org_id']
        user_permission = request.session['user_permission']
        cost_model_license = request.session['cost_model_license']
        osparc_license = request.session['osparc_license']
        sessionid = request.session['sessionid']
        csrftoken = request.session['csrftoken']
    except KeyError:
        return redirect('auth')

    cookies = {'sessionid': sessionid, 'csrftoken': csrftoken}
    headers = {'content-type': 'application/json', 'X-CSRFToken': csrftoken}

    api = APIWrapper.from_session(sessionid)
    is_superuser = api.is_superuser

    plants_req = requests.get(BASE_URL+'/organizations/'+str(org_id)+'/no-pagination-plants/?format=json', cookies=cookies)
    plants = plants_req.json()['data']

    plant_groups_req = requests.get(BASE_URL + '/organizations/' + str(org_id) + '/plant_groups/?format=json',
                                    cookies=cookies)
    plant_groups = plant_groups_req.json()

    costmodels_req = requests.get(BASE_URL + '/organizations/' + str(org_id) + '/cost_models/?format=json', cookies=cookies)
    costmodels = costmodels_req.json()

    req = requests.get(BASE_URL + '/organizations/'+str(org_pk)+'/plant_groups/'+str(pk)+'/?format=json&depth=1',
                       cookies=cookies)
    plant_group_detail_data = req.json()

    plants_in_group_req = requests.get(BASE_URL+'/organizations/'+str(org_pk)+'/plant_groups/'+str(pk)+'/?format=json',
                                       cookies=cookies)
    plants_in_group = plants_in_group_req.json()
    plant_list = plants_in_group['plant']
    plant_group_list = plants_in_group['plant_group']

    if req.status_code == 404:
        raise Http404("Plant Group does not exist")

    if request.method == 'POST' and 'plant_group_del_btn' in request.POST:
        plant_group_del_req = requests.delete(BASE_URL+'/organizations/'+str(org_pk)+'/plant_groups/'+str(pk)+'/?format=json',
                                              headers=headers, cookies=cookies)
        print(plant_group_del_req.status_code)
        if plant_group_del_req.status_code == 204:
            return redirect('/organizations/'+str(org_id)+'/plant_groups/')

    elif request.method == 'POST' and 'plant_group_update_btn' in request.POST:
        name_ip = request.POST.get('name_ip')
        desc_ip = request.POST.get('desc_ip')
        plants_ip = request.POST.getlist('plants_ip')
        plant_groups_ip = request.POST.getlist('plant_groups_ip')
        costmodel_ip = request.POST.get('costmodel_ip')
        plant_list_ip = request.POST.getlist('plant_list_ip')

        plant_group_update_data = {'name': name_ip, 'description': desc_ip, 'plant': plants_ip,
                                   'plant_group': plant_groups_ip, 'cost_model': costmodel_ip,
                                   'plant_list': plant_list_ip,  'organization': org_id}
        plant_group_update_req = requests.put(BASE_URL+'/organizations/'+str(org_pk)+'/plant_groups/'+str(pk)+'/?format=json',
                                              headers=headers, data=json.dumps(plant_group_update_data), cookies=cookies)

        if plant_group_update_req.status_code == 200:
            messages.success(request, "Success! Plant Group Updated. ")
            req = requests.get(BASE_URL+'/organizations/'+str(org_pk)+'/plant_groups/'+str(pk)+'/?depth=1&format=json',
                               cookies=cookies)
            plant_group_detail_data = req.json()
            plants_in_group_req = requests.get(
                BASE_URL + '/organizations/' + str(org_pk) + '/plant_groups/' + str(pk) + '/?format=json',
                cookies=cookies)
            plants_in_group = plants_in_group_req.json()
            plant_list = plants_in_group['plant']
            plant_group_list = plants_in_group['plant_group']
        else:
            messages.warning(request, "Error! - " + str(plant_group_update_req.content) + " - Please try again.")

    return render(request, 'costmodel/plant_group_detail.html',
                  {
                      'org_id': org_id, 'user_permission': user_permission, 'plants': plants,
                      'plant_group_detail': plant_group_detail_data, 'plant_list': plant_list,
                      'plant_groups': plant_groups, 'plant_group_list': plant_group_list, 'costmodels': costmodels,
                      'cost_model_license': cost_model_license, 'osparc_license': osparc_license,
                      'is_superuser': is_superuser
                  })
