from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [

    url(r'^organizations/(?P<org_pk>\d+)/plant_groups/$', views.plant_group_list, name='plant_group_list'),
    url(r'^organizations/(?P<org_pk>\d+)/plant_groups/(?P<pk>\d+)$', views.plant_group_detail, name='plant_group_detail'),
    url(r'^plant_group_create/$', views.plant_group_create, name='plant_group_create'),

    url(r'^reports/$', views.reports, name='reports'),
    url(r'^organizations/(?P<org_pk>\d+)/reports/$', views.reports_list, name='reports-list'),
    url(r'^report_detail/(?P<pk>\d+)$', views.reports_detail, name='report-detail'),
    url(r'^group_report_detail/(?P<pk>\d+)$', views.group_reports_detail, name='group_reports_detail'),

    url(r'^organizations/(?P<org_pk>\d+)/costmodels/$', views.costmodel_list, name='costmodel_list'),
    url(r'^organizations/(?P<org_pk>\d+)/costmodels/(?P<pk>\d+)$', views.costmodel_detail, name='costmodel_detail'),
    url(r'^costmodel_create/$', views.costmodel_create, name='costmodel_create'),

    url(r'^service_create/$', views.service_create, name='service_create'),
    url(r'^organizations/(?P<org_pk>\d+)/services/$', views.service_list, name='service_list'),
    url(r'^organizations/(?P<org_pk>\d+)/services/(?P<pk>\d+)$', views.service_detail, name='service_detail'),

    url(r'^organizations/(?P<org_pk>\d+)/labor_rates/$', views.labor_rates, name='labor_rates'),
    url(r'^organizations/(?P<org_pk>\d+)/labor_rates/(?P<pk>\d+)$', views.labor_rate_detail, name='labor_rate_detail'),
    url(r'^labor_rates/new/$', views.labor_rates_create, name='labor_rates_create'),
    url(r'^costmodels-dropdown/$', views.costmodels_dropdown, name='costmodels-dropdown'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
